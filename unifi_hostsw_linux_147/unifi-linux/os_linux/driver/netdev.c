/*
 * ---------------------------------------------------------------------------
 * FILE:     netdev.c
 *
 * PURPOSE:
 *      This file provides the upper edge interface to the linux netdevice
 *      and wireless extensions.
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include <linux/types.h>
#include <linux/etherdevice.h>
#include <linux/vmalloc.h>
#include "driver/unifi.h"
#include "driver/conversions.h"
#include "unifi_priv.h"
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,13)
#include <net/ieee80211.h>
#else
#include <net/iw_handler.h>
#endif
#include <net/pkt_sched.h>

#ifndef P80211_OUI_LEN
#define P80211_OUI_LEN  3
#endif
#ifndef ETH_P_PAE
#define ETH_P_PAE 0x888e
#endif


#define ieee2host16(n)  __le16_to_cpu(n)
#define ieee2host32(n)  __le32_to_cpu(n)
#define host2ieee16(n)  __cpu_to_le16(n)
#define host2ieee32(n)  __cpu_to_le32(n)

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
#ifdef UNIFI_NET_NAME
#define UF_ALLOC_NETDEV(_dev, _size, _name, _setup, _num_of_queues)     \
    do {                                                                \
        static uint8 name[8];                                           \
        sprintf(name, "%s%s", UNIFI_NET_NAME, _name);                   \
        _dev = alloc_netdev_mq(_size, name, _setup, _num_of_queues);    \
    } while (0);
#else
#define UF_ALLOC_NETDEV(_dev, _size, _name, _setup, _num_of_queues)     \
    do {                                                                \
        _dev = alloc_etherdev_mq(_size, _num_of_queues);                \
    } while (0);
#endif /* UNIFI_NET_NAME */
#else
#ifdef UNIFI_NET_NAME
#define UF_ALLOC_NETDEV(_dev, _size, _name, _setup, _num_of_queues)     \
    do {                                                                \
        static uint8 name[8];                                           \
        sprintf(name, "%s%s", UNIFI_NET_NAME, _name);                   \
        _dev = alloc_netdev(_size, name, _setup);                       \
    } while (0);
#else
#define UF_ALLOC_NETDEV(_dev, _size, _name, _setup, _num_of_queues)     \
    do {                                                                \
        _dev = alloc_etherdev(_size);                                   \
    } while (0);
#endif /* UNIFI_NET_NAME */
#endif /* LINUX_VERSION_CODE */


typedef struct {
    u8    dsap;   /* always 0xAA */
    u8    ssap;   /* always 0xAA */
    u8    ctrl;   /* always 0x03 */
    u8    oui[P80211_OUI_LEN];    /* organizational universal id */
    u16 protocol;
} __attribute__ ((packed)) llc_snap_hdr_t;

/* Wext handler is suported only if CSR_SUPPORT_WEXT is defined */
#ifdef CSR_SUPPORT_WEXT
extern struct iw_handler_def unifi_iw_handler_def;
#endif /* CSR_SUPPORT_WEXT */

static int unifi_net_open(struct net_device *dev);
static int unifi_net_ioctl(struct net_device *dev, struct ifreq *rq, int cmd);
static int unifi_net_stop(struct net_device *dev);
static struct net_device_stats *unifi_net_get_stats(struct net_device *dev);
static int unifi_net_xmit(struct sk_buff *skb, struct net_device *dev);
static void unifi_set_multicast_list(struct net_device *dev);
static unifi_PortAction verify_port(unifi_priv_t *priv, unsigned char *address);


#ifdef CONFIG_NET_SCHED
/*
 * Queueing Discipline Interface
 * Only used if kernel is configured with CONFIG_NET_SCHED
 */

/*
 * The driver uses the qdisc interface to buffer and control all
 * outgoing traffic. We create a root qdisc, register our qdisc operations
 * and later we create two subsiduary pfifo queues for the uncontrolled
 * and controlled ports.
 *
 * The network stack delivers all outgoing packets in our enqueue handler.
 * There, we classify the packet and decide whether to store it or drop it
 * (if the controlled port state is set to "discard").
 * If the packet is enqueued, the network stack call our dequeue handler.
 * There, we decide whether we can send the packet, delay it or drop it
 * (the controlled port configuration might have changed meanwhile).
 * If a packet is dequeued, then the network stack calls our hard_start_xmit
 * handler where finally we send the packet.
 *
 * If the hard_start_xmit handler fails to send the packet, we return
 * NETDEV_TX_BUSY and the network stack call our requeue handler where
 * we put the packet back in the same queue in came from.
 *
 */
#define UF_QDISK_MAX_QUEUES 2
#define UF_QDISK_UNCONTROLLED_QUEUE     0
#define UF_QDISK_CONTROLLED_QUEUE       1

struct uf_sched_data
{
    /* Traffic Classifier TBD */
    struct tcf_proto *filter_list;
    /* Our two queues */
    struct Qdisc *queues[UF_QDISK_MAX_QUEUES];
};


struct uf_tx_packet_data {
    /* Store the queue the packet is stored to */
    unsigned int queue;
    /* Debug */
    unsigned long host_tag;
};

static int uf_qdiscop_enqueue(struct sk_buff *skb, struct Qdisc* qd);
static int uf_qdiscop_requeue(struct sk_buff *skb, struct Qdisc* qd);
static struct sk_buff *uf_qdiscop_dequeue(struct Qdisc* qd);
static void uf_qdiscop_reset(struct Qdisc* qd);
static void uf_qdiscop_destroy(struct Qdisc* qd);
static int uf_qdiscop_dump(struct Qdisc *qd, struct sk_buff *skb);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,26)
static int uf_qdiscop_tune(struct Qdisc *qd, struct nlattr *opt);
static int uf_qdiscop_init(struct Qdisc *qd, struct nlattr *opt);
#else
static int uf_qdiscop_tune(struct Qdisc *qd, struct rtattr *opt);
static int uf_qdiscop_init(struct Qdisc *qd, struct rtattr *opt);
#endif


/* queueing discipline operations */
static struct Qdisc_ops uf_qdisc_ops =
{
    .next = NULL,
    .cl_ops = NULL,
    .id = "UniFi Qdisc",
    .priv_size = sizeof(struct uf_sched_data),

    .enqueue = uf_qdiscop_enqueue,
    .dequeue = uf_qdiscop_dequeue,
    .requeue = uf_qdiscop_requeue,
    .drop = NULL, /* drop not needed since we are always the root qdisc */

    .init = uf_qdiscop_init,
    .reset = uf_qdiscop_reset,
    .destroy = uf_qdiscop_destroy,
    .change = uf_qdiscop_tune,

    .dump = uf_qdiscop_dump,
};


#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
#define UF_QDISC_CREATE_DFLT(_dev, _ops, _root)         \
    qdisc_create_dflt(dev, netdev_get_tx_queue(_dev, 0), _ops, _root)
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,20)
#define UF_QDISC_CREATE_DFLT(_dev, _ops, _root)         \
    qdisc_create_dflt(dev, _ops, _root)
#else
#define UF_QDISC_CREATE_DFLT(_dev, _ops, _root)         \
    qdisc_create_dflt(dev, _ops)
#endif /* LINUX_VERSION_CODE */

#endif /* CONFIG_NET_SCHED */


static u8 oui_rfc1042[P80211_OUI_LEN] = { 0x00, 0x00, 0x00 };
static u8 oui_8021h[P80211_OUI_LEN]   = { 0x00, 0x00, 0xf8 };


/* Callback for event logging to blocking clients */
static void netdev_mlme_event_handler(ul_client_t  *client,
                                      u8 *sig_packed, int sig_len,
                                      const bulk_data_param_t *bulkdata,
                                      int dir);



/*
 * ---------------------------------------------------------------------------
 *  unifi_alloc_netdevice
 *
 *      Allocate memory for the net_device and device private structs
 *      for this interface.
 *      Fill in the fields, but don't register the interface yet.
 *      We need to configure the UniFi first.
 *
 *  Arguments:
 *      sdio_dev        Pointer to SDIO context handle to use for all
 *                      SDIO ops.
 *      bus_id          A small number indicating the SDIO card position on the
 *                      bus. Typically this is the slot number, e.g. 0, 1 etc.
 *                      Valid values are 0 to MAX_UNIFI_DEVS-1.
 *
 *  Returns:
 *      Pointer to device private struct.
 *
 *  Notes:
 *      The net_device and device private structs are allocated together
 *      and should be freed by freeing the net_device pointer.
 * ---------------------------------------------------------------------------
 */
unifi_priv_t *
unifi_alloc_netdevice(void *sdio_dev, int bus_id)
{
    struct net_device *dev;
    unifi_priv_t *priv;

    /*
     * Allocate netdevice struct, assign name template and
     * setup as an ethernet device.
     * The net_device and private structs are zeroed. Ether_setup() then
     * sets up ethernet handlers and values.
     * The RedHat 9 redhat-config-network tool doesn't recognise wlan* devices,
     * so use "eth*" (like other wireless extns drivers).
     */
    UF_ALLOC_NETDEV(dev, sizeof(unifi_priv_t), "%d", ether_setup, 1);
    if (dev == NULL) {
        return NULL;
    }


    /* Set up back pointer from priv to netdev */
    priv = netdev_priv(dev);
    priv->netdev = dev;


    /* Setup / override net_device fields */
    dev->open             = unifi_net_open;
    dev->stop             = unifi_net_stop;
    dev->hard_start_xmit  = unifi_net_xmit;
    dev->do_ioctl         = unifi_net_ioctl;

    /* called by /proc/net/dev */
    dev->get_stats = unifi_net_get_stats;

    dev->set_multicast_list = unifi_set_multicast_list;

#ifdef CSR_SUPPORT_WEXT
    dev->wireless_handlers = &unifi_iw_handler_def;
#if IW_HANDLER_VERSION < 6
    dev->get_wireless_stats = unifi_get_wireless_stats;
#endif /* IW_HANDLER_VERSION */
#endif /* CSR_SUPPORT_WEXT */

    /* Use bus_id as instance number */
    priv->instance = bus_id;
    /* Store SDIO pointer to pass in the core */
    priv->sdio = sdio_dev;

    /* Consider UniFi to be uninitialised */
    priv->init_progress = UNIFI_INIT_NONE;

    /*
     * Initialise the clients structure array.
     * We do not need protection around ul_init_clients() because
     * the character device can not be used until unifi_alloc_netdevice()
     * returns and Unifi_instances[bus_id]=priv is set, since unifi_open()
     * will return -ENODEV.
     */
    ul_init_clients(priv);

    /* Register a new ul client to send the multicast list signals. */
    priv->netdev_client = ul_register_client(priv,
                                             0,
                                             netdev_mlme_event_handler);
    if (priv->netdev_client == NULL) {
        unifi_error(priv,
                    "Failed to register a unifi client for background netdev processing\n");
        free_netdev(priv->netdev);
        return NULL;
    }
    unifi_trace(priv, UDBG2, "Netdev client (id:%d s:0x%X) is registered\n",
                priv->netdev_client->client_id, priv->netdev_client->sender_id);

    priv->sta_wmm_capabilities = 0;

    /*
     * Initialise the OS private struct.
     */
    /*
     * Instead of deciding in advance to use 11bg or 11a, we could do a more
     * clever scan on both radios.
     */
    if (use_5g) {
        priv->if_index = CSR_INDEX_5G;
        unifi_info(priv, "Using the 802.11a radio\n");
    } else {
        priv->if_index = CSR_INDEX_2G4;
    }

    /* Initialise bh thread structure */
    priv->bh_thread.thread_task = NULL;
    init_waitqueue_head(&priv->bh_thread.wakeup_q);
    priv->bh_thread.wakeup_flag = 0;
    unifi_sprintf(priv->bh_thread.name, "uf_bh_thread");

    priv->connected = UnifiConnectedUnknown;  /* -1 unknown, 0 no, 1 yes */

#ifdef USE_DRIVER_LOCK
    init_MUTEX(&priv->lock);
#endif /* USE_DRIVER_LOCK */

    spin_lock_init(&priv->send_signal_lock);

    /* Create the Traffic Analysis workqueue */
    priv->unifi_workqueue = create_singlethread_workqueue("unifi_workq");
    if (priv->unifi_workqueue == NULL) {
        /* Deregister priv->netdev_client */
        ul_deregister_client(priv->netdev_client);
        free_netdev(priv->netdev);
        return NULL;
    }
    /* Create the Multicast Addresses list work structure */
    INIT_WORK(&priv->multicast_list_task, uf_multicast_list_wq);

#ifdef CONFIG_NET_SCHED
    /* Register the qdisc operations */
    register_qdisc(&uf_qdisc_ops);
#endif /* CONFIG_NET_SCHED */

    priv->ref_count = 1;


    priv->amp_client = NULL;

    return priv;
} /* unifi_alloc_netdevice() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_free_netdevice
 *
 *      Unregister the network device and free the memory allocated for it.
 *      NB This includes the memory for the priv struct.
 *
 *  Arguments:
 *      priv            Device private pointer.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int
unifi_free_netdevice(unifi_priv_t *priv)
{
    func_enter();

    if (!priv) {
        return -EINVAL;
    }

    /*
     * The buffers for holding f/w images were allocated using vmalloc
     * so must be freed with vfree.
     */
    if (priv->fw_loader.dl_data) {
        vfree(priv->fw_loader.dl_data);
    }
    priv->fw_loader.dl_data = NULL;
    priv->fw_loader.dl_len = 0;
    if (priv->fw_sta.dl_data) {
        vfree(priv->fw_sta.dl_data);
    }
    priv->fw_sta.dl_data = NULL;
    priv->fw_sta.dl_len = 0;

#if (defined CSR_SUPPORT_SME) && (defined CSR_SUPPORT_WEXT)
    if (priv->connection_config.mlmeAssociateReqInformationElements.data) {
        kfree(priv->connection_config.mlmeAssociateReqInformationElements.data);
    }
    priv->connection_config.mlmeAssociateReqInformationElements.data = NULL;
    priv->connection_config.mlmeAssociateReqInformationElements.length = 0;

    if (priv->mib_data.length) {
        vfree(priv->mib_data.data);
    }
    priv->mib_data.data = NULL;
    priv->mib_data.length = 0;

#endif /* CSR_SUPPORT_SME && CSR_SUPPORT_WEXT*/


#ifdef CONFIG_NET_SCHED
    /* Unregister the qdisc operations */
    unregister_qdisc(&uf_qdisc_ops);
#endif /* CONFIG_NET_SCHED */

    /* Cancel work items and destroy the workqueue */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,23)
    cancel_work_sync(&priv->multicast_list_task);
#endif
    flush_workqueue(priv->unifi_workqueue);
    destroy_workqueue(priv->unifi_workqueue);

    /* Free the netdev struct and priv, which are all one lump. */
    free_netdev(priv->netdev);

    func_exit();
    return 0;
} /* unifi_free_netdevice() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_net_open
 *
 *      Called when userland does "ifconfig wlan0 up".
 *
 *  Arguments:
 *      dev             Device pointer.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
static int
unifi_net_open(struct net_device *dev)
{
    unifi_priv_t *priv = netdev_priv(dev);

    func_enter();

    /* If we haven't finished UniFi initialisation, we can't start */
    if (priv->init_progress != UNIFI_INIT_COMPLETED) {
        unifi_trace(priv, UDBG1, "%s: unifi not ready, failing net_open\n", __FUNCTION__);
        return -EINVAL;
    }

#if (defined CSR_NATIVE_LINUX) && (defined UNIFI_SNIFF_ARPHRD)
    /* 
     * To sniff, the user must do "iwconfig mode monitor", which sets
     * priv->wext_conf.mode to IW_MODE_MONITOR.
     * Then he/she must do "ifconfig ethn up", which calls this fn.
     * There is no point in starting the sniff with SNIFFJOIN until
     * this point.
     */
    if (priv->wext_conf.mode == IW_MODE_MONITOR) {
        int err;
        err = uf_start_sniff(priv);
        if (err) {
            return err;
        }
        netif_carrier_on(dev);
    }
#endif

    UF_NETIF_TX_START_ALL_QUEUES(dev);

    func_exit();
    return 0;
} /* unifi_net_open() */


static int
unifi_net_stop(struct net_device *dev)
{
#ifdef CSR_NATIVE_LINUX
    unifi_priv_t *priv = netdev_priv(dev);
#endif

    func_enter();

#ifdef CSR_NATIVE_LINUX
    /* Stop sniffing if in Monitor mode */
    if (priv->wext_conf.mode == IW_MODE_MONITOR) {
        if (priv->card) {
            int err;
            err = unifi_reset_state(priv, dev->dev_addr, 1);
            if (err) {
                return err;
            }
        }
    }
#endif

    UF_NETIF_TX_STOP_ALL_QUEUES(dev);

    func_exit();
    return 0;
} /* unifi_net_stop() */


/* This is called after the WE handlers */
static int
unifi_net_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
    int rc;

    rc = -EOPNOTSUPP;

    return rc;
} /* unifi_net_ioctl() */



static struct net_device_stats *
unifi_net_get_stats(struct net_device *dev)
{
    unifi_priv_t *priv = netdev_priv(dev);

    return &priv->stats;
} /* unifi_net_get_stats() */



static int
skb_ether_to_80211(struct net_device *dev, struct sk_buff *skb, int proto)
{
    llc_snap_hdr_t *snap;

    /* step 1: classify ether frame, DIX or 802.3? */

    if (proto < 0x600) {
        /* codes <= 1500 reserved for 802.3 lengths */
        /* it's 802.3, pass ether payload unchanged,  */
        unifi_trace(netdev_priv(dev), UDBG3, "802.3 len: %d\n", skb->len);

        /*   leave off any PAD octets.  */
        skb_trim(skb, proto);
    } else if (proto == ETH_P_8021Q) {
        /* Store the VLAN SNAP (should be 87-65). */
        u16 vlan_snap = *(u16*)skb->data;
        /* Add AA-AA-03-00-00-00 */
        snap = (llc_snap_hdr_t *)skb_push(skb, 4);
        snap->dsap = snap->ssap = 0xAA;
        snap->ctrl = 0x03;
        memcpy(snap->oui, oui_rfc1042, P80211_OUI_LEN);

        /* Add AA-AA-03-00-00-00 */
        snap = (llc_snap_hdr_t *)skb_push(skb, 10);
        snap->dsap = snap->ssap = 0xAA;
        snap->ctrl = 0x03;
        memcpy(snap->oui, oui_rfc1042, P80211_OUI_LEN);

        /* Add the VLAN specific information */
        snap->protocol = htons(proto);
        *(u16*)(snap + 1) = vlan_snap;

    } else {
        /* it's DIXII, time for some conversion */
        unifi_trace(netdev_priv(dev), UDBG3, "DIXII len: %d\n", skb->len);

        /* tack on SNAP */
        snap = (llc_snap_hdr_t *)skb_push(skb, sizeof(llc_snap_hdr_t));
        snap->dsap = snap->ssap = 0xAA;
        snap->ctrl = 0x03;
        /* Use the appropriate OUI. */
        if ((proto == ETH_P_AARP) || (proto == ETH_P_IPX)) {
            memcpy(snap->oui, oui_8021h, P80211_OUI_LEN);
        } else {
            memcpy(snap->oui, oui_rfc1042, P80211_OUI_LEN);
        }
        snap->protocol = htons(proto);
    }

    return 0;
} /* skb_ether_to_80211() */


static int
indicate_amp_unidata(ul_client_t *client, uint16 proto, u8 *signal,
                     int signal_len, bulk_data_param_t *bulkdata)
{
    int filter_index;

    for (filter_index = 0; filter_index < client->snap_filter.count; filter_index++) {
        if (proto == client->snap_filter.protocols[filter_index]) {
            /* Send to client */
            client->event_hook(client,
                               signal, signal_len,
                               bulkdata, UDI_TO_HOST);

            return 1;
        }
    }

    return 0;
}


/*
 * ---------------------------------------------------------------------------
 *  skb_80211_to_ether
 *
 *      Make sure the received frame is in Ethernet (802.3) form.
 *      De-encapsulates SNAP if necessary, adds a ethernet header.
 *
 *  Arguments:
 *      payload         Pointer to packet data received from UniFi.
 *      payload_length  Number of bytes of data received from UniFi.
 *      daddr           Destination MAC address.
 *      saddr           Source MAC address.
 *
 *  Returns:
 *      0 on success, -1 if the packet is bad and should be dropped,
 *      1 if the packet was forwarded to the SME or AMP client.
 * ---------------------------------------------------------------------------
 */
static int
skb_80211_to_ether(unifi_priv_t *priv, struct sk_buff *skb,
                   const unsigned char *daddr, const unsigned char *saddr,
                   u8 *packed_signal, int signal_len,
                   CSR_SIGNAL *signal, bulk_data_param_t *bulkdata)
{
    unsigned char *payload;
    int payload_length;
    struct ethhdr *eth;
    llc_snap_hdr_t *snap;
#define UF_VLAN_LLC_HEADER_SIZE     18
    static const u8 vlan_inner_snap[] = { 0xAA, 0xAA, 0x03, 0x00, 0x00, 0x00 };

    payload = skb->data;
    payload_length = skb->len;

    snap = (llc_snap_hdr_t *)payload;
    eth  = (struct ethhdr *)payload;

    /*
     * Test for the various encodings
     */
    if ((payload_length >= sizeof(llc_snap_hdr_t)) &&
        (snap->dsap == 0xAA) &&
        (snap->ssap == 0xAA) &&
        (snap->ctrl == 0x03) &&
        (snap->oui[0] == 0) &&
        (snap->oui[1] == 0) &&
        ((snap->oui[2] == 0) || (snap->oui[2] == 0xF8)))
    {
        /* AppleTalk AARP (2) or IPX SNAP */
        if ((snap->oui[2] == 0) &&
             ((ntohs(snap->protocol) == ETH_P_AARP) || (ntohs(snap->protocol) == ETH_P_IPX)))
        {
            u16 len;

            unifi_trace(priv, UDBG3, "%s len: %d\n",
                  (ntohs(snap->protocol) == ETH_P_AARP) ? "ETH_P_AARP" : "ETH_P_IPX",
                  payload_length);
            /* Add 802.3 header and leave full payload */
            len = htons(skb->len);
            memcpy(skb_push(skb, 2), &len, 2);
            memcpy(skb_push(skb, ETH_ALEN), saddr, ETH_ALEN);
            memcpy(skb_push(skb, ETH_ALEN), daddr, ETH_ALEN);

            return 0;
        }
        /* VLAN-tagged IP */
        if ((snap->oui[2] == 0) && (ntohs(snap->protocol) == ETH_P_8021Q))
        {
            /*
             * The translation doesn't change the packet length, so is done in-place.
             *
             * Example header (from Std 802.11-2007 Annex M):
             * AA-AA-03-00-00-00-81-00-87-65-AA-AA-03-00-00-00-08-06
             * -------SNAP-------p1-p1-ll-ll-------SNAP--------p2-p2
             * dd-dd-dd-dd-dd-dd-aa-aa-aa-aa-aa-aa-p1-p1-ll-ll-p2-p2
             * dd-dd-dd-dd-dd-dd-aa-aa-aa-aa-aa-aa-81-00-87-65-08-06
             */
            u16 vlan_snap;

            if (payload_length < UF_VLAN_LLC_HEADER_SIZE) {
                unifi_warning(priv, "VLAN SNAP header too short: %d bytes\n", payload_length);
                return -1;
            }

            if (memcmp(payload + 10, vlan_inner_snap, 6)) {
                unifi_warning(priv, "VLAN malformatted SNAP header.\n");
                return -1;
            }

            unifi_trace(priv, UDBG3, "VLAN SNAP: %02x-%02x\n", payload[8], payload[9]);
            unifi_trace(priv, UDBG3, "VLAN len: %d\n", payload_length);

            /* Create the 802.3 header */

            vlan_snap = *((u16*)(payload + 8));

            /* Create LLC header without byte-swapping */
            eth->h_proto = snap->protocol;

            memcpy(eth->h_dest, daddr, ETH_ALEN);
            memcpy(eth->h_source, saddr, ETH_ALEN);
            *(u16*)(eth + 1) = vlan_snap;
            return 0;
        }

        /* it's a SNAP + RFC1042 frame */
        unifi_trace(priv, UDBG3, "SNAP+RFC1042 len: %d\n", payload_length);

        /* chop SNAP+llc header from skb. */
        skb_pull(skb, sizeof(llc_snap_hdr_t));

        /* create 802.3 header at beginning of skb. */
        eth = (struct ethhdr *)skb_push(skb, ETH_HLEN);
        memcpy(eth->h_dest, daddr, ETH_ALEN);
        memcpy(eth->h_source, saddr, ETH_ALEN);
        /* Copy protocol field without byte-swapping */
        eth->h_proto = snap->protocol;
    } else {
        static const u8 bt_oui[] = {0x00, 0x19, 0x58};
        int indicated;

        if (!memcmp(snap->oui, bt_oui, 3)) {

            indicated = 0;
            if (priv->amp_client) {
                indicated = indicate_amp_unidata(priv->amp_client,
                                                 ntohs(snap->protocol),
                                                 packed_signal,
                                                 signal_len,
                                                 bulkdata);
            }

            if (!indicated && priv->sme_cli) {
                indicate_amp_unidata(priv->sme_cli,
                                     ntohs(snap->protocol),
                                     packed_signal,
                                     signal_len,
                                     bulkdata);
            }

            unifi_net_data_free(priv, &bulkdata->d[0]);

            return 1;
        } else {
            u16 len;

            /* Add 802.3 header and leave full payload */
            len = htons(skb->len);
            memcpy(skb_push(skb, 2), &len, 2);
            memcpy(skb_push(skb, ETH_ALEN), saddr, ETH_ALEN);
            memcpy(skb_push(skb, ETH_ALEN), daddr, ETH_ALEN);
        }
    }

    return 0;
} /* skb_80211_to_ether() */


/*
 * ---------------------------------------------------------------------------
 *  send_ma_unidata_request
 *  send_mlme_eapol_request
 *
 *      These functions send a data packet to UniFi for transmission.
 *      EAP protocol packets are sent using send_mlme_eapol_request(),
 *      all other packets are sent using send_ma_unidata_request().
 * 
 *  Arguments:
 *      priv            Pointer to device private context struct
 *      skb             Socket buffer containing data packet to transmit
 *      ehdr            Pointer to Ethernet header within skb.
 *
 *  Returns:
 *      Zero on success or error code.
 * ---------------------------------------------------------------------------
 */
static int
send_ma_unidata_request(unifi_priv_t *priv, struct sk_buff *skb, const struct ethhdr *ehdr)
{
    CSR_SIGNAL signal;
    CSR_MA_UNITDATA_REQUEST *req = &signal.u.MaUnitdataRequest;
    bulk_data_param_t bulkdata;
    int r;
    const int proto = ntohs(ehdr->h_proto);
    static unsigned long tag = 1;

    /*
     * Priority and ServiceClass are QoS items.
     * If the packet is not unicast we send it as non-QoS.
     */
    if (((priv->sta_wmm_capabilities & QOS_CAPABILITY_WMM_ENABLED) == 0) ||
        ((ehdr->h_dest[0] & 0x01) == 0x01)) {
        req->Priority = CSR_CONTENTION;
        req->ServiceClass = CSR_REORDERABLE_MULTICAST;
    }
    else {
        req->Priority = skb->priority >> 5;

        if (req->Priority == 0) {
            switch (proto) {
              case 0x0800:        /* IPv4 */
              case 0x814C:        /* SNMP */
              case 0x880C:        /* GSMP */
                req->Priority = (CSR_PRIORITY) (skb->data[1] >> 5);
                break;

              case 0x8100:        /* VLAN */
                req->Priority = (CSR_PRIORITY) (skb->data[0] >> 5);
                break;

              case 0x86DD:        /* IPv6 */
                req->Priority = (CSR_PRIORITY) ((skb->data[0] & 0x0E) >> 1);
                break;

              default:
                req->Priority = 0;
                break;
            }
        }
        req->ServiceClass = CSR_QOS_ACK;
    }

    /* Remove the ethernet header and add a SNAP header if necessary */
    if (skb_ether_to_80211(priv->netdev, skb, proto) != 0) {
        /* convert failed */
        unifi_error(priv, "ether_to_80211 failed.\n");
        return 1;
    }
    unifi_trace(priv, UDBG3, "Tx Frame with Priority: %d\n", req->Priority);

#if 0
    printk("after SNAP hdr:\n");
    dump(skb->data, (skb->len > 32) ? 32 : skb->len);
#endif

    memcpy(req->Da.x, ehdr->h_dest, ETH_ALEN);
    memcpy(req->Sa.x, ehdr->h_source, ETH_ALEN);

#if 0
    printk("MA-UNITDATA.req: %d bytes\n", skb->len);
    printk(" Da %02X:%02X:%02X:%02X:%02X:%02X, Sa %02X:%02X:%02X:%02X:%02X:%02X\n",
           req.Da.x[0], req.Da.x[1], req.Da.x[2],
           req.Da.x[3], req.Da.x[4], req.Da.x[5],
           req.Sa.x[0], req.Sa.x[1], req.Sa.x[2],
           req.Sa.x[3], req.Sa.x[4], req.Sa.x[5]);
#endif

    req->RoutingInformation = CSR_NULL_RT; /* always set to zero */

    req->HostTag = tag++;

    signal.SignalPrimitiveHeader.SignalId = CSR_MA_UNITDATA_REQUEST_ID;
    bulkdata.d[0].os_data_ptr = skb->data;
    bulkdata.d[0].os_net_buf_ptr = (unsigned char*)skb;
    bulkdata.d[0].data_length = skb->len;
    bulkdata.d[1].os_data_ptr = NULL;
    bulkdata.d[1].os_net_buf_ptr = NULL;
    bulkdata.d[1].data_length = 0;

#ifdef CSR_SUPPORT_SME
    /* Notify the TA module for the Tx frame */
    unifi_ta_sample(priv->card, unifi_traffic_tx,
                    &bulkdata.d[0], ehdr->h_source,
                    priv->netdev->dev_addr,
                    jiffies_to_msecs(jiffies));
#endif /* CSR_SUPPORT_SME */

    /* Send UniFi msg */
    r = unifi_ma_unitdata(priv, priv->netdev_client, &signal, &bulkdata);
    unifi_trace(priv, UDBG3, "UNITDATA result code = %d\n", r);

    return r;
} /* send_ma_unidata_request() */


static int
send_mlme_eapol_request(unifi_priv_t *priv, struct sk_buff *skb, const struct ethhdr *ehdr)
{
    CSR_SIGNAL signal;
    CSR_MLME_EAPOL_REQUEST *req = &signal.u.MlmeEapolRequest;
    bulk_data_param_t bulkdata;
    int r;
    int proto = ntohs(ehdr->h_proto);

    /* Remove the ethernet header and add a SNAP header if necessary */
    if (skb_ether_to_80211(priv->netdev, skb, proto) != 0) {
        /* convert failed */
        unifi_error(priv, "ether_to_80211 failed.\n");
        return 1;
    }

#if 0
    printk("after SNAP hdr:\n");
    dump(skb->data, (skb->len > 32) ? 32 : skb->len);
#endif

    memcpy(req->Da.x, &ehdr->h_dest, ETH_ALEN);
    memcpy(req->Sa.x, &ehdr->h_source, ETH_ALEN);

    signal.SignalPrimitiveHeader.SignalId = CSR_MLME_EAPOL_REQUEST_ID;
    bulkdata.d[0].os_data_ptr = skb->data;
    bulkdata.d[0].os_net_buf_ptr = (unsigned char*)skb;
    bulkdata.d[0].data_length = skb->len;
    bulkdata.d[1].os_data_ptr = NULL;
    bulkdata.d[1].os_net_buf_ptr = NULL;
    bulkdata.d[1].data_length = 0;

#ifdef CSR_SUPPORT_SME
    /* Notify the TA module for the Tx frame */
    unifi_ta_sample(priv->card, unifi_traffic_tx,
                    &bulkdata.d[0], ehdr->h_source,
                    priv->netdev->dev_addr,
                    jiffies_to_msecs(jiffies));
#endif /* CSR_SUPPORT_SME */

    /* Send to UniFi. This does not block for the reply. */
    r = unifi_mlme_eapol(priv, priv->netdev_client, &signal, &bulkdata);
    unifi_trace(priv, UDBG3, "EAPOL result code = %d\n", r);

    return r;
} /* send_mlme_eapol_request() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_net_xmit
 *
 *      This function is called by the higher level stack to transmit an
 *      ethernet packet.
 *
 *  Arguments:
 *      skb     Ethernet packet to send.
 *      dev     Pointer to the linux net device.
 *
 *  Returns:
 *      Zero on success, non-zero if unable to transmit the packet.
 *
 *  Notes:
 *      The controlled port is handled in the qdisc dequeue handler.
 * ---------------------------------------------------------------------------
 */
static int
unifi_net_xmit(struct sk_buff *skb, struct net_device *dev)
{
    unifi_priv_t *priv = netdev_priv(dev);
    struct ethhdr ehdr;
    int proto;
    int result;
#ifndef CONFIG_NET_SCHED
    unifi_PortAction cp_action;
#endif /* CONFIG_NET_SCHED */

    func_enter();

#if 0
    printk("xmit %d bytes\n", skb->len);
    dump(skb->data, skb->len);
#endif

    /* Remove the ethernet header */
    memcpy(&ehdr, skb->data, ETH_HLEN);
    skb_pull(skb, ETH_HLEN);

    proto = ntohs(ehdr.h_proto);

#if 0
    if (proto == ETH_P_PAE) {
        printk("EAPOL, %d bytes\n", skb->len);
    }
#endif

    if (proto == ETH_P_PAE) {
        result = send_mlme_eapol_request(priv, skb, &ehdr);
    } else {

#ifdef CONFIG_NET_SCHED
        /* Send the packet, port is open */
        result = send_ma_unidata_request(priv, skb, &ehdr);
#else
        cp_action = verify_port(priv, ehdr.h_dest);

        if (cp_action == unifi_8021x_PortOpen) {
            result = send_ma_unidata_request(priv, skb, &ehdr);
        } else {

            /* Discard or block the packet if necessary */
            if (cp_action == unifi_8021x_PortClosedBlock) {
                /* We can not send the packet now, put it back to the queue */
                unifi_trace(priv, UDBG5,
                            "unifi_net_xmit: packet requeued, controlled port blocked\n");
                result = 1;
            } else {
                unifi_trace(priv, UDBG5,
                            "unifi_net_xmit: packet dropped, controlled port closed\n");
                priv->stats.tx_dropped++;
                kfree_skb(skb);

                func_exit();
                return 0;
            }

        }
#endif /* CONFIG_NET_SCHED */

    }

    if (result == 0) {

        dev->trans_start = jiffies;

        /*
         * Should really count tx stats in the UNITDATA.status signal but
         * that doesn't have the length.
         */
        priv->stats.tx_packets++;
        /* count only the packet payload */
        priv->stats.tx_bytes += skb->len;

    } else {
        unifi_trace(priv, UDBG5, "Tx returns NETDEV_TX_BUSY\n");
        result = 1;     /* NETDEV_TX_BUSY */
    }

    /*
     * Do not free the skb buffer, if the packet was delivered to the core
     * it will be freed when it is send, otherwise the system will requeue it.
     */

    func_exit();
    return result;
} /* unifi_net_xmit() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_pause_xmit
 *  unifi_restart_xmit
 *
 *      These functions are called from the UniFi core to control the flow
 *      of packets from the upper layers.
 *      unifi_pause_xmit() is called when the internal queue is full and
 *      should take action to stop unifi_ma_unitdata() being called.
 *      When the queue has drained, unifi_restart_xmit() will be called to
 *      re-enable the flow of packets for transmission.
 *
 *  Arguments:
 *      ospriv          OS private context pointer.
 *
 *  Returns:
 *      unifi_pause_xmit() is called from interrupt context.
 * ---------------------------------------------------------------------------
 */
void
unifi_pause_xmit(void *ospriv)
{
    unifi_priv_t *priv = ospriv;

    unifi_trace(priv, UDBG2, "Stopping netif\n");
    if (netif_running(priv->netdev)) {
        UF_NETIF_TX_STOP_ALL_QUEUES(priv->netdev);
    }
} /* unifi_pause_xmit() */


void
unifi_restart_xmit(void *ospriv)
{
    unifi_priv_t *priv = ospriv;

    unifi_trace(priv, UDBG2, "Waking netif\n");
    if (netif_running(priv->netdev)) {
        UF_NETIF_TX_WAKE_ALL_QUEUES(priv->netdev);
    }
} /* unifi_restart_xmit() */



/*
 * ---------------------------------------------------------------------------
 *  uf_resume_xmit
 *
 *      Is called when the controlled port is set to open,
 *      to notify the network stuck to schedule for transmission
 *      any packets queued in the qdisk while port was closed.
 *
 *  Arguments:
 *      priv        Pointer to device private struct
 *
 *  Returns:
 * ---------------------------------------------------------------------------
 */
void
uf_resume_xmit(unifi_priv_t *priv)
{
    unifi_trace(priv, UDBG2, "Resuming netif\n");

#ifdef CONFIG_NET_SCHED
    if (netif_running(priv->netdev)) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
        netif_schedule_queue(netdev_get_tx_queue(priv->netdev, 0));
#else
        netif_schedule(priv->netdev);
#endif /* LINUX_VERSION_CODE */
    }
#endif

} /* uf_resume_xmit() */


static unifi_PortAction verify_port(unifi_priv_t *priv, unsigned char *address)
{
#ifdef CSR_NATIVE_LINUX
    return priv->wext_conf.block_controlled_port;
#else
    return uf_sme_controlled_port_state(priv, address);
#endif
}



/*
 * ---------------------------------------------------------------------------
 *  unifi_rx
 *
 *      Reformat a UniFi data received packet into a p80211 packet and
 *      pass it up the protocol stack.
 *
 *  Arguments:
 *      None.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
static void
unifi_rx(unifi_priv_t *priv, u8 *packed_signal, int signal_len,
         CSR_SIGNAL *signal, bulk_data_param_t *bulkdata)
{
    struct net_device *dev = priv->netdev;
    const CSR_MA_UNITDATA_INDICATION *ind = &signal->u.MaUnitdataIndication;
    struct sk_buff *skb;
    struct ethhdr *eth;
    unifi_PortAction controlled_port;
    int r;

    func_enter();

    if (bulkdata->d[0].data_length == 0) {
        unifi_warning(priv, "rx: MA-UNITDATA indication with zero bulk data\n");
        func_exit();
        return;
    }

#ifdef CSR_SUPPORT_SME
    /* Notify the TA module for the Rx frame */
    unifi_ta_sample(priv->card, unifi_traffic_rx,
                    &bulkdata->d[0],
                    ind->Sa.x,
                    priv->netdev->dev_addr,
                    jiffies_to_msecs(jiffies));
#endif /* CSR_SUPPORT_SME */

    skb = (struct sk_buff*)bulkdata->d[0].os_net_buf_ptr;
    skb->len = bulkdata->d[0].data_length;

    /*
     * De-encapsulate any SNAP header and
     * prepend an ethernet header so that the skb manipulation and ARP
     * stuff works.
     */
    r = skb_80211_to_ether(priv, skb, ind->Da.x, ind->Sa.x,
                           packed_signal, signal_len,
                           signal, bulkdata);
    if (r)
    {
        if (r == -1) {
            /* Drop the packet and return */
            priv->stats.rx_errors++;
            priv->stats.rx_frame_errors++;
            unifi_net_data_free(priv, &bulkdata->d[0]);
            unifi_notice(priv, "unifi_rx: Discard unknown frame.\n");
        }
        return;
    }

    controlled_port = verify_port(priv, (unsigned char*)ind->Sa.x);

    /* Now we look like a regular ethernet frame */

    /* Apply 802.11X controlled port */
    eth  = (struct ethhdr *)skb->data;
    if ((ntohs(eth->h_proto) != ETH_P_PAE) &&
        (controlled_port != unifi_8021x_PortOpen)) {
        /* Drop the packet and return */
        priv->stats.rx_dropped++;
        unifi_notice(priv, "unifi_rx: Dropping non-EAPOL packet, proto=0x%04x, port=%d\n",
                     ntohs(eth->h_proto), controlled_port);
        unifi_net_data_free(priv, &bulkdata->d[0]);
        return;
    }


    /* Test for an overlength frame */
    if (skb->len > (dev->mtu + ETH_HLEN)) {
        /* A bogus length ethfrm has been encap'd. */
        /* Is someone trying an oflow attack? */
        unifi_error(priv, "%s: oversize frame (%d > %d)\n",
                    dev->name,
                    skb->len, dev->mtu + ETH_HLEN);

        /* Drop the packet and return */
        priv->stats.rx_errors++;
        priv->stats.rx_length_errors++;
        unifi_net_data_free(priv, &bulkdata->d[0]);
        return;
    }


    /* Fill in SKB meta data */
    skb->dev = dev;
    skb->protocol = eth_type_trans(skb, dev);
    skb->ip_summed = CHECKSUM_UNNECESSARY;


    /* Pass SKB up the stack */
    netif_rx(skb);
    dev->last_rx = jiffies;

    /* Bump the rx stats */
    priv->stats.rx_packets++;
    priv->stats.rx_bytes += bulkdata->d[0].data_length;

    func_exit();

} /* unifi_rx() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_set_multicast_list
 *
 *      This function is called by the higher level stack to set
 *      a list of multicast rx addresses.
 *
 *  Arguments:
 *      dev             Network Device pointer.
 *
 *  Returns:
 *      None.
 *
 *  Notes:
 * ---------------------------------------------------------------------------
 */

static void
unifi_set_multicast_list(struct net_device *dev)
{
    unifi_priv_t *priv = netdev_priv(dev);
    struct dev_mc_list *p;      /* Pointer to the addresses structure. */
    int i;
    u8 *mc_list = priv->mc_list;

    if (priv->init_progress != UNIFI_INIT_COMPLETED) {
        return;
    }

    unifi_trace(priv, UDBG3,
                "unifi_set_multicast_list (count=%d)\n", dev->mc_count);

    /* Not enough space? */
    if (dev->mc_count > UNIFI_MAX_MULTICAST_ADDRESSES) {
        return;
    }

    /* Store the list to be processed by the work item. */
    priv->mc_list_count = dev->mc_count;
    p = dev->mc_list;
    for (i = 0; i < dev->mc_count; i++) {
        memcpy(mc_list, p->dmi_addr, ETH_ALEN);
        p = p->next;
        mc_list += ETH_ALEN;
    }

    /* Send a message to the workqueue */
    queue_work(priv->unifi_workqueue, &priv->multicast_list_task);

} /* unifi_set_multicast_list() */



/*
 * ---------------------------------------------------------------------------
 *  netdev_mlme_event_handler
 *
 *      Callback function to be used as the udi_event_callback when registering
 *      as a netdev client.
 *      To use it, a client specifies this function as the udi_event_callback
 *      to ul_register_client(). The signal dispatcher in
 *      unifi_receive_event() will call this function to deliver a signal.
 *
 *  Arguments:
 *      pcli            Pointer to the client instance.
 *      signal          Pointer to the received signal.
 *      signal_len      Size of the signal structure in bytes.
 *      bulkdata        Pointer to structure containing any associated bulk data.
 *      dir             Direction of the signal. Zero means from host,
 *                      non-zero means to host.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
static void
netdev_mlme_event_handler(ul_client_t *pcli,
                          u8 *sig_packed, int sig_len,
                          const bulk_data_param_t *bulkdata_o,
                          int dir)
{
    CSR_SIGNAL signal;
    unifi_priv_t *priv = unifi_find_instance(pcli->instance);
    int id, r;
    bulk_data_param_t bulkdata;

    func_enter();

    /* Just a sanity check */
    if (sig_packed == NULL) {
        return;
    }

    /* 
     * This copy is to silence a compiler warning about discarding the
     * const qualifier.
     */
    bulkdata = *bulkdata_o;

    /* Get the unpacked signal */
    r = read_unpack_signal(sig_packed, &signal);
    if (r) {
        unifi_error(priv, "Received unknown or corrupted signal.\n");
        return;
    }

    id = signal.SignalPrimitiveHeader.SignalId;
    unifi_trace(priv, UDBG3, "Netdev - Process signal 0x%X %s\n", id, lookup_signal_name(id));

    /*
     * Take the appropriate action for the signal.
     */
    switch (id) {
      case CSR_MA_UNITDATA_INDICATION_ID:
        unifi_rx(priv, sig_packed, sig_len, &signal, &bulkdata);
        break;

      case CSR_MA_UNITDATA_CONFIRM_ID:
        unifi_ma_unitdata_status_ind(priv,
                                     &signal.u.MaUnitdataConfirm);
        break;

      case CSR_DEBUG_STRING_INDICATION_ID:
        debug_string_indication(priv, bulkdata.d[0].os_data_ptr, bulkdata.d[0].data_length);
        break;

      case CSR_DEBUG_WORD16_INDICATION_ID:
        debug_word16_indication(priv, &signal);
        break;

      case CSR_DEBUG_GENERIC_CONFIRM_ID:
      case CSR_DEBUG_GENERIC_INDICATION_ID:
        debug_generic_indication(priv, &signal);
        break;

      default:
        break;
    }

    func_exit();
} /* netdev_mlme_event_handler() */



#ifdef CONFIG_NET_SCHED
/*
 * ---------------------------------------------------------------------------
 *  uf_install_qdisc
 *
 *      Creates a root qdisc, registers our qdisc handlers and
 *      overrides the device's qdisc_sleeping to prevent the system
 *      from creating a new one for our network device.
 *
 *  Arguments:
 *      dev             Pointer to the network device.
 *
 *  Returns:
 *      0 on success, Linux error code otherwise.
 *
 *  Notes:
 *      This function holds the qdisk lock so it needs to be called
 *      after registering the network device in uf_register_netdev().
 *      Also, the qdisc_create_dflt() API has changed in 2.6.20 to
 *      include the parentid.
 * ---------------------------------------------------------------------------
 */
int uf_install_qdisc(struct net_device *dev)
{
    struct Qdisc *qdisc;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
    struct netdev_queue *queue0;
#endif /* LINUX_VERSION_CODE */

    qdisc = UF_QDISC_CREATE_DFLT(dev, &uf_qdisc_ops, TC_H_ROOT);
    if (!qdisc) {
        unifi_error(NULL, "%s: qdisc installation failed\n", dev->name);
        return -EFAULT;
    }

    /*
     * The following code is copied from the mac80211 code in the
     * linux kernel. The usage of the handle is still not very clear.
     */
    qdisc->handle = 0x80020000;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
    queue0 = netdev_get_tx_queue(dev, 0);
    if (queue0 == NULL) {
        unifi_error(NULL, "%s: netdev_get_tx_queue returned no queue\n", dev->name);
        return -EFAULT;
    }
    queue0->qdisc = qdisc;
    queue0->qdisc_sleeping = qdisc;
#else
    qdisc_lock_tree(dev);
    list_add_tail(&qdisc->list, &dev->qdisc_list);
    dev->qdisc_sleeping = qdisc;
    qdisc_unlock_tree(dev);
#endif

    return 0;

} /* uf_install_qdisc() */



static int uf_qdiscop_enqueue(struct sk_buff *skb, struct Qdisc* qd)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
    unifi_priv_t *priv = netdev_priv(qd->dev_queue->dev);
#else
    unifi_priv_t *priv = netdev_priv(qd->dev);
#endif /* LINUX_VERSION_CODE */
    struct uf_sched_data *q = qdisc_priv(qd);
    struct uf_tx_packet_data *pkt_data = (struct uf_tx_packet_data *) skb->cb;
    struct ethhdr ehdr;
    struct Qdisc *qdisc;
    int r, queue, proto;

    memcpy(&ehdr, skb->data, ETH_HLEN);
    proto = ntohs(ehdr.h_proto);

    /* 802.1x - apply controlled/uncontrolled port rules */
    if (proto != ETH_P_PAE) {
        queue = UF_QDISK_CONTROLLED_QUEUE;
    } else {
        queue = UF_QDISK_UNCONTROLLED_QUEUE;
    }

    pkt_data->queue = (unsigned int)queue;


    qdisc = q->queues[queue];
    r = qdisc->enqueue(skb, qdisc);
    if (r == NET_XMIT_SUCCESS) {
        qd->q.qlen++;
        qd->bstats.bytes += skb->len;
        qd->bstats.packets++;
        return NET_XMIT_SUCCESS;
    }

    unifi_error(priv, "uf_qdiscop_enqueue: dropped\n");
    qd->qstats.drops++;

    return r;

} /* uf_qdiscop_enqueue() */


static int uf_qdiscop_requeue(struct sk_buff *skb, struct Qdisc* qd)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
    unifi_priv_t *priv = netdev_priv(qd->dev_queue->dev);
#else
    unifi_priv_t *priv = netdev_priv(qd->dev);
#endif /* LINUX_VERSION_CODE */
    struct uf_sched_data *q = qdisc_priv(qd);
    struct uf_tx_packet_data *pkt_data = (struct uf_tx_packet_data *) skb->cb;
    struct Qdisc *qdisc;
    int r;

    unifi_trace(priv, UDBG5, "uf_qdiscop_requeue: (q=%d), tag=%u\n",
                pkt_data->queue, pkt_data->host_tag);

    /* we recorded which queue to use earlier! */
    qdisc = q->queues[pkt_data->queue];

    if ((r = qdisc->ops->requeue(skb, qdisc)) == 0) {
        qd->q.qlen++;
        return 0;
    }

    unifi_error(priv, "uf_qdiscop_requeue: dropped\n");
    qd->qstats.drops++;

    return r;
} /* uf_qdiscop_requeue() */


static struct sk_buff *uf_qdiscop_dequeue(struct Qdisc* qd)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
    unifi_priv_t *priv = qd->dev_queue->dev->priv;
#else
    unifi_priv_t *priv = qd->dev->priv;
#endif /* LINUX_VERSION_CODE */
    struct uf_sched_data *q = qdisc_priv(qd);
    struct sk_buff *skb;
    struct Qdisc *qdisc;
    int queue;
    struct ethhdr ehdr;
    struct uf_tx_packet_data *pkt_data;
    unifi_PortAction cp_action;

    /* check all the queues */
    for (queue = 0; queue < UF_QDISK_MAX_QUEUES; queue++) {

        qdisc = q->queues[queue];
        skb = qdisc->dequeue(qdisc);
        if (skb) {
            /* A packet has been dequeued, decrease the queued packets count */
            qd->q.qlen--;

            pkt_data = (struct uf_tx_packet_data *) skb->cb;

            /* The controlled port queue requires special handling */
            if (queue == UF_QDISK_CONTROLLED_QUEUE) {
                /* Check the controlled port status */
                memcpy(&ehdr, skb->data, ETH_HLEN);
                cp_action = verify_port(priv, ehdr.h_dest);

                /* Discard or block the packet if necessary */
                if (cp_action == unifi_8021x_PortClosedDiscard) {
                    unifi_trace(priv, UDBG5, "uf_qdiscop_dequeue: drop (q=%d), tag=%u\n",
                                pkt_data->queue, pkt_data->host_tag);
                    kfree_skb(skb);
                    break;
                }

                if (cp_action == unifi_8021x_PortClosedBlock) {
                    /* We can not send the packet now, put it back to the queue */
                    if (qdisc->ops->requeue(skb, qdisc) != 0) {
                        unifi_error(priv, "uf_qdiscop_dequeue: requeue (q=%d) failed, tag=%u, drop it\n",
                                    pkt_data->queue, pkt_data->host_tag);

                        /* Requeue failed, drop the packet */
                        kfree_skb(skb);
                        break;
                    }
                    /* We requeued the packet, increase the queued packets count */
                    qd->q.qlen++;

                    unifi_trace(priv, UDBG5, "uf_qdiscop_dequeue: skip (q=%d), tag=%u\n",
                                pkt_data->queue, pkt_data->host_tag);
                    continue;
                }
            }

            unifi_trace(priv, UDBG5, "uf_qdiscop_dequeue: new (q=%d), tag=%u\n",
                        pkt_data->queue, pkt_data->host_tag);

            return skb;
        }
    }

    return NULL;
} /* uf_qdiscop_dequeue() */


static void uf_qdiscop_reset(struct Qdisc* qd)
{
    struct uf_sched_data *q = qdisc_priv(qd);
    int queue;

    func_enter();

    for (queue = 0; queue < UF_QDISK_MAX_QUEUES; queue++) {
        qdisc_reset(q->queues[queue]);
    }
    qd->q.qlen = 0;

    func_exit();
} /* uf_qdiscop_reset() */


static void uf_qdiscop_destroy(struct Qdisc* qd)
{
    struct uf_sched_data *q = qdisc_priv(qd);
    int queue;

    func_enter();

    for (queue=0; queue < UF_QDISK_MAX_QUEUES; queue++) {
        qdisc_destroy(q->queues[queue]);
        q->queues[queue] = &noop_qdisc;
    }

    func_exit();
} /* uf_qdiscop_destroy() */


/* called whenever parameters are updated on existing qdisc */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,26)
static int uf_qdiscop_tune(struct Qdisc *qd, struct nlattr *opt)
#else
static int uf_qdiscop_tune(struct Qdisc *qd, struct rtattr *opt)
#endif
{
    return 0;
} /* uf_qdiscop_tune() */


/* called during initial creation of qdisc on device */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,26)
static int uf_qdiscop_init(struct Qdisc *qd, struct nlattr *opt)
#else
static int uf_qdiscop_init(struct Qdisc *qd, struct rtattr *opt)
#endif
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
    struct net_device *dev = qd->dev_queue->dev;
    unifi_priv_t *priv = netdev_priv(dev);
#else
    struct net_device *dev = qd->dev;
    unifi_priv_t *priv = netdev_priv(dev);
#endif /* LINUX_VERSION_CODE */
    struct uf_sched_data *q = qdisc_priv(qd);
    int err = 0, i;

    /*
     * check that there is no qdisc currently attached to device
     * this ensures that we will be the root qdisc. (I can't find a better
     * way to test this explicitly) 
     */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27)
    if (dev->qdisc_sleeping != &noop_qdisc) {
        return -EINVAL;
    }
#endif /* LINUX_VERSION_CODE */

    /* make sure we do not mess with the ingress qdisc */
    if (qd->flags & TCQ_F_INGRESS) {
        return -EINVAL;
    }

    /* if options were passed in, set them */
    if (opt) {
        err = uf_qdiscop_tune(qd, opt);
    }

    /* create child queues */
    for (i = 0; i < UF_QDISK_MAX_QUEUES; i++) {
        q->queues[i] = UF_QDISC_CREATE_DFLT(dev, &pfifo_qdisc_ops,
                                            qd->handle);
        if (!q->queues[i]) {
            q->queues[i] = &noop_qdisc;
            unifi_error(priv, "%s child qdisc %i creation failed\n");
        }
    }

    return err;
} /* uf_qdiscop_init() */


static int uf_qdiscop_dump(struct Qdisc *qd, struct sk_buff *skb)
{
    return skb->len;
} /* uf_qdiscop_dump() */

#endif /* CONFIG_NET_SCHED */

