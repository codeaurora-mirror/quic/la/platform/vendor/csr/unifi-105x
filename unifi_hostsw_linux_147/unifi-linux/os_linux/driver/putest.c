/*
 * ***************************************************************************
 *  FILE:     putest.c
 * 
 *  PURPOSE:    putest related functions.
 * 
 *  Copyright (C) 2008 by Cambridge Silicon Radio Ltd.
 *  
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ***************************************************************************
 */

#include <linux/vmalloc.h>
#include <linux/firmware.h>

#include "unifi_priv.h"


int unifi_putest_cmd52_read(unifi_priv_t *priv, unsigned char *arg)
{
    struct unifi_putest_cmd52 cmd52_params;
    uint8 *arg_pos;
    unsigned int cmd_param_size;
    int r;
    unsigned char ret_buffer[32];
    uint8 *ret_buffer_pos;

    arg_pos = (uint8*)(((unifi_putest_command_t*)arg) + 1);
    if (get_user(cmd_param_size, (int*)arg_pos)) {
        unifi_error(priv,
                    "unifi_putest_cmd52_read: Failed to get the argument\n");
        return -EFAULT;
    }

    if (cmd_param_size != sizeof(struct unifi_putest_cmd52)) {
        unifi_error(priv,
                    "unifi_putest_cmd52_read: cmd52 struct mismatch\n");
        return -EINVAL;
    }

    arg_pos += sizeof(unsigned int);
    if (copy_from_user(&cmd52_params,
                       (void*)arg_pos,
                       sizeof(struct unifi_putest_cmd52))) {
        unifi_error(priv,
                    "unifi_putest_cmd52_read: Failed to get the cmd52 params\n");
        return -EFAULT;
    }

    unifi_trace(priv, UDBG2, "cmd52r: func=%d addr=0x%x ",
                cmd52_params.funcnum, cmd52_params.addr);

    r = unifi_sdio_readb(priv->sdio, cmd52_params.funcnum,
                         cmd52_params.addr, &cmd52_params.data);
    if (r)
    {
        unifi_error(priv,
                    "\nunifi_putest_cmd52_read: unifi_sdio_readb() failed (r=0x%x)\n", r);
        return r;
    }
    unifi_trace(priv, UDBG2, "data=%d\n", cmd52_params.data);

    /* Copy the info to the out buffer */
    *(unifi_putest_command_t*)ret_buffer = UNIFI_PUTEST_CMD52_READ;
    ret_buffer_pos = (uint8*)(((unifi_putest_command_t*)ret_buffer) + 1);
    *(unsigned int*)ret_buffer_pos = sizeof(struct unifi_putest_cmd52);
    ret_buffer_pos += sizeof(unsigned int);
    memcpy(ret_buffer_pos, &cmd52_params, sizeof(struct unifi_putest_cmd52));
    ret_buffer_pos += sizeof(struct unifi_putest_cmd52);

    r = copy_to_user((void*)arg,
                     ret_buffer,
                     ret_buffer_pos - ret_buffer);
    if (r) {
        unifi_error(priv,
                    "unifi_putest_cmd52_read: Failed to return the data\n");
        return -EFAULT;
    }

    return 0;
}


int unifi_putest_cmd52_write(unifi_priv_t *priv, unsigned char *arg)
{
    struct unifi_putest_cmd52 cmd52_params;
    uint8 *arg_pos;
    unsigned int cmd_param_size;
    int r;

    arg_pos = (uint8*)(((unifi_putest_command_t*)arg) + 1);
    if (get_user(cmd_param_size, (int*)arg_pos)) {
        unifi_error(priv,
                    "unifi_putest_cmd52_write: Failed to get the argument\n");
        return -EFAULT;
    }

    if (cmd_param_size != sizeof(struct unifi_putest_cmd52)) {
        unifi_error(priv,
                    "unifi_putest_cmd52_write: cmd52 struct mismatch\n");
        return -EINVAL;
    }

    arg_pos += sizeof(unsigned int);
    if (copy_from_user(&cmd52_params,
                       (void*)(arg_pos),
                       sizeof(struct unifi_putest_cmd52))) {
        unifi_error(priv,
                    "unifi_putest_cmd52_write: Failed to get the cmd52 params\n");
        return -EFAULT;
    }

    unifi_trace(priv, UDBG2, "cmd52w: func=%d addr=0x%x data=%d\n",
                cmd52_params.funcnum, cmd52_params.addr, cmd52_params.data);

    r = unifi_sdio_writeb(priv->sdio, cmd52_params.funcnum,
                          cmd52_params.addr, cmd52_params.data);
    if (r)
    {
        unifi_error(priv,
                    "unifi_putest_cmd52_write: unifi_sdio_writeb() failed (r=0x%x)\n", r);
    }

    return r;
}


int unifi_putest_set_sdio_clock(unifi_priv_t *priv, unsigned char *arg)
{
    int sdio_clock_speed;
    int r;

    if (get_user(sdio_clock_speed, (int*)(((unifi_putest_command_t*)arg) + 1))) {
        unifi_error(priv,
                    "unifi_putest_set_sdio_clock: Failed to get the argument\n");
        return -EFAULT;
    }

    unifi_trace(priv, UDBG2, "set sdio clock: %d KHz\n", sdio_clock_speed);

    r = unifi_sdio_set_max_clock_speed(priv->sdio, sdio_clock_speed);
    if (r != sdio_clock_speed)
    {
        unifi_error(priv,
                    "unifi_putest_set_sdio_clock: Set clock failed (r=0x%x)\n", r);
        return r;
    }

    return 0;
}


int unifi_putest_start(unifi_priv_t *priv, unsigned char *arg)
{
    int r;

    /* Suspend the SME and UniFi */
    r = sme_sys_suspend(priv);
    if (r) {
        unifi_error(priv,
                    "unifi_putest_start: failed to suspend UniFi\n");
        return r;
    }

    /* Power on UniFi */
    r = unifi_sdio_power_on(priv->sdio);
    if (r < 0) {
        unifi_error(priv,
                    "unifi_putest_start: failed to power on UniFi\n");
        return r;
    }

    r = unifi_init(priv->card);
    if (r && (r != 1)) {
        unifi_error(priv,
                    "unifi_putest_start: failed to init UniFi\n");
        return r;
    }

    return 0;
}


int unifi_putest_stop(unifi_priv_t *priv, unsigned char *arg)
{
    int r;

    /* Power off UniFi */
    unifi_sdio_power_off(priv->sdio);

    /* Resume the SME and UniFi */
    r = sme_sys_resume(priv);
    if (r) {
        unifi_error(priv,
                    "unifi_putest_stop: failed to resume UniFi\n");
    }

    return r;
}


int unifi_putest_dl_fw(unifi_priv_t *priv, unsigned char *arg)
{
#define UF_PUTEST_MAX_FW_FILE_NAME      16
#define UNIFI_MAX_FW_PATH_LEN           32
    unsigned int fw_name_length;
    unsigned char fw_name[UF_PUTEST_MAX_FW_FILE_NAME];
    unsigned char *name_buffer;
    int postfix;
    char fw_path[UNIFI_MAX_FW_PATH_LEN];
    const struct firmware *fw_entry;
    struct dlpriv temp_fw_sta;
    int r;

    /* Get the f/w file name length */
    if (get_user(fw_name_length, (unsigned int*)(((unifi_putest_command_t*)arg) + 1))) {
        unifi_error(priv,
                    "unifi_putest_dl_fw: Failed to get the length argument\n");
        return -EFAULT;
    }

    unifi_trace(priv, UDBG2, "unifi_putest_dl_fw: file name size = %d\n", fw_name_length);

    /* Sanity check for the f/w file name length */
    if (fw_name_length > UF_PUTEST_MAX_FW_FILE_NAME) {
        unifi_error(priv,
                    "unifi_putest_dl_fw: F/W file name is too long\n");
        return -EINVAL;
    }

    /* Get the f/w file name */
    name_buffer = ((unsigned char*)arg) + sizeof(unifi_putest_command_t) + sizeof(unsigned int);
    if (copy_from_user(fw_name, (void*)name_buffer, fw_name_length)) {
        unifi_error(priv, "unifi_putest_dl_fw: Failed to get the file name\n");
        return -EFAULT;
    }

    /* Keep the existing f/w to a temp, we need to restore it later */
    temp_fw_sta = priv->fw_sta;

    /* Get the putest f/w */
    postfix = priv->instance;
    scnprintf(fw_path, UNIFI_MAX_FW_PATH_LEN, "unifi-sdio-%d/%s",
              postfix, fw_name);
    r = request_firmware(&fw_entry, fw_path, priv->unifi_device);
    if (r == 0) {
        priv->fw_sta.dl_data = vmalloc(fw_entry->size);
        if (priv->fw_sta.dl_data == NULL)
        {
            unifi_error(priv,
                        "Failed to allocate memory for firmware image\n");
            goto restore_fw;
        }

        memcpy(priv->fw_sta.dl_data, fw_entry->data, fw_entry->size);
        priv->fw_sta.dl_len = fw_entry->size;
        release_firmware(fw_entry);
    } else {
        unifi_error(priv, "Firmware file not available\n");
        return -EINVAL;
    }

    /* Download the f/w */
    r = unifi_download(priv->card, 0x0c00);
    if (r < 0) {
        unifi_error(priv,
                    "unifi_putest_dl_fw: failed to download the f/w\n");
        goto free_fw;
    }

    /* Free the putest f/w... */
free_fw:
    vfree(priv->fw_sta.dl_data);

    /* ... and restore the original f/w */
restore_fw:
    priv->fw_sta = temp_fw_sta;

    return r;
}




