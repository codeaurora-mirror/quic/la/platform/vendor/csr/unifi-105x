/*
 * ---------------------------------------------------------------------------
 * FILE:     data_tx.c
 * 
 * PURPOSE:
 *      This file provides functions to send data requests to the UniFi.
 *
 * Copyright (C) 2007-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include "driver/unifi.h"
#include "unifi_priv.h"


/*
 * ---------------------------------------------------------------------------
 *
 *      Data transport signals.
 *
 * ---------------------------------------------------------------------------
 */

/*
 * ---------------------------------------------------------------------------
 * unifi_mlme_eapol
 *
 *      Send a EAP data packet in a MLME-EAPOL signal to UniFi.
 *
 * Arguments:
 *      priv            Pointer to device private context struct
 *      pcli            Pointer to context of calling process
 *      sig             Pointer to a signal containing a MLME-EAPOL.req
 *      bulkdata        Pointer to a bulk data structure, describing
 *                      the data to be sent.
 *
 * Returns:
 *      0 on success
 *      -1 if an error occurred
 * ---------------------------------------------------------------------------
 */
int
unifi_mlme_eapol(unifi_priv_t *priv, ul_client_t *pcli,
                 CSR_SIGNAL *sig, const bulk_data_param_t *bulkdata)
{
    int r;

    sig->SignalPrimitiveHeader.ReceiverProcessId = 0;
    sig->SignalPrimitiveHeader.SenderProcessId = pcli->sender_id;

    /* Send the signal to UniFi */
    r = ul_send_signal_unpacked(priv, sig, bulkdata);
    if (r) {
        unifi_error(priv, "Error queueing CSR_MLME_EAPOL_REQUEST signal\n");
        return -1;
    }

    /*
     * We do not advance the sequence number of the last sent signal
     * because we will not wait for a response from UniFi.
     */

    return 0;
} /* unifi_mlme_eapol() */


/* 
 * ---------------------------------------------------------------------------
 * unifi_ma_unitdata
 * unifi_ds_unitdata
 * 
 *      Send a UNITDATA signal to UniFi.
 *
 * Arguments:
 *      priv            Pointer to device private context struct
 *      pcli            Pointer to context of calling process
 *      sig             Pointer to a signal containing a MA_UNITDATA 
 *                      or DS_UNITDATA request structure to send.
 *      bulkdata        Pointer a to bulk data structure, describing
 *                      the data to be sent.
 *
 * Returns:
 *      0 on success
 *      -1 if an error occurred
 * ---------------------------------------------------------------------------
 */
int
unifi_ma_unitdata(unifi_priv_t *priv, ul_client_t *pcli,
                  CSR_SIGNAL *sig, const bulk_data_param_t *bulkdata)
{
    int r;

    sig->SignalPrimitiveHeader.ReceiverProcessId = 0;
    sig->SignalPrimitiveHeader.SenderProcessId = pcli->sender_id;

    /* Send the signal to UniFi */
    r = ul_send_signal_unpacked(priv, sig, bulkdata);
    if (r) {
        unifi_error(priv, "Error queueing CSR_MA_UNITDATA_REQUEST signal\n");
        return -1;
    }

    /*
     * We do not advance the sequence number of the last sent signal
     * because we will not wait for a response from UniFi.
     */

    return 0;
} /* unifi_ma_unitdata() */

int
unifi_ds_unitdata(unifi_priv_t *priv, ul_client_t *pcli,
                  CSR_SIGNAL *sig, const bulk_data_param_t *bulkdata)
{
    int r;

    sig->SignalPrimitiveHeader.ReceiverProcessId = 0;
    sig->SignalPrimitiveHeader.SenderProcessId = pcli->sender_id;

    /* Send the signal to UniFi */
    r = ul_send_signal_unpacked(priv, sig, bulkdata);
    if (r) {
        unifi_error(priv, "Error queueing CSR_DS_UNITDATA_REQUEST signal\n");
        return -1;
    }

    /*
     * We do not advance the sequence number of the last sent signal
     * because we will not wait for a response from UniFi.
     */

    return 0;
} /* unifi_ds_unitdata() */


