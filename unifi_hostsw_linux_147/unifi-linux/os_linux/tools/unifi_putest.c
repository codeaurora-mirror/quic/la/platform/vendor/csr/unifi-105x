/*
 * ***************************************************************************
 *
 *  FILE: unifi_config.c
 *
 *      Get/Set configuration to the UniFi driver.
 *
 * Copyright (C) 2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ***************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <getopt.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <stddef.h>

#include "osa_types.h"
#include "unifiio.h"

#include "unifi_putest_lib.h"


int read16(int fd, unsigned int address, uint16 *value)
{
    struct unifi_putest_cmd52 cmd52_params;
    unsigned char lo = 0xAD, hi = 0xDE;
    int r;

    cmd52_params.funcnum = 1;
    cmd52_params.addr = address;
    cmd52_params.data = lo;
    r = unifi_putest_cmd52_read(fd, &cmd52_params);
    if (r < 0) {
        printf("UNIFI_PUTEST_CMD52_READ: failed\n");
        return r;
    }

    lo = cmd52_params.data;

    cmd52_params.funcnum = 1;
    cmd52_params.addr = address + 1;
    cmd52_params.data = hi;
    r = unifi_putest_cmd52_read(fd, &cmd52_params);
    if (r < 0) {
        printf("UNIFI_PUTEST_CMD52_READ: failed\n");
        return r;
    }

    hi = cmd52_params.data;
    *value = (((uint16)hi) << 8) | lo;

    return 0;
}


int write16(int fd, unsigned int address, uint16 value)
{
    struct unifi_putest_cmd52 cmd52_params;
    int r;

    cmd52_params.funcnum = 1;
    cmd52_params.addr = address + 1;
    cmd52_params.data = (unsigned char)(value >> 8);
    r = unifi_putest_cmd52_write(fd, &cmd52_params);
    if (r < 0) {
        printf("UNIFI_PUTEST_CMD52_WRITE: failed\n");
        return r;
    }

    cmd52_params.funcnum = 1;
    cmd52_params.addr = address;
    cmd52_params.data = (unsigned char)value;
    r = unifi_putest_cmd52_write(fd, &cmd52_params);
    if (r < 0) {
        printf("UNIFI_PUTEST_CMD52_WRITE: failed\n");
        return r;
    }

    return 0;
}


static int Cmd52Test(int fd)
{
    uint16 reply;
    int n, r;

    /* select MAC processor DBG_HOST_PROC_SELECT */
    r = write16(fd, 0x1fd20, 0 );
    if (r) {
        printf("CMD52 Test Failed\n");
        return -1;
    }

    /* set up memory window 0x233 */
    r = write16(fd, 0x1fcf0, 0x233);
    if (r) {
        printf("CMD52 Test Failed\n");
        return -1;
    }

    for ( n = 0; n < 10; n++ )
    {
        uint16 data = (0x06AD << n) + n;
        r = write16(fd, 0x4000 + 0x0fb0, data);
        if (r) {
            printf("CMD52 Test Failed\n");
            return -1;
        }

        r = read16(fd, 0x4000 + 0x0fb0, &reply);
        if (r) {
            printf("CMD52 Test Failed\n");
            return -1;
        }

        if (data != reply)
        {
            printf("Fail at %d, expected: %04x observed: %04x\n", n, data, reply);
            return -1;
        }
    }

    printf("CMD52 Test Succeed\n");
    return 0;
}


int
main(int argc, char **argv)
{
    char device[] = "/dev/unifiudi0";
    int r;
    int fd;
    int clock_khz;

    fd = unifi_putest_init(device);
    if (fd < 0) {
        perror("Failed to open device");
        exit(errno);
    }

    r = unifi_putest_start(fd);
    if (r < 0) {
        printf("UNIFI_PUTEST_START: failed\n");
        goto EXIT;
    }

    sleep(1);

    clock_khz = 25000;
    r = unifi_putest_set_sdio_clock(fd, clock_khz);
    if (r < 0) {
        printf("UNIFI_PUTEST_SDIO_CLOCK: failed\n");
        goto EXIT;
    }

    r = Cmd52Test(fd);
    if (r < 0) {
        goto EXIT;
    }

    r = unifi_putest_dl_fw(fd, "putest_sta.xbv", 14);
    if (r < 0) {
        printf("UNIFI_PUTEST_DL_FW: failed\n");
        goto EXIT;
    }

    r = Cmd52Test(fd);
    if (r < 0) {
        goto EXIT;
    }

EXIT:
    r = unifi_putest_stop(fd);
    if (r < 0) {
        printf("UNIFI_PUTEST_STOP: failed\n");
    }

    /* Kill communication with driver. */
    unifi_putest_deinit(fd);

    return r;
} /* main() */

