/** @file event_pack_unpack.h
 *
 * Event Packing and unpacking functions
 *
 * @section LEGAL
 *   CONFIDENTIAL
 *
 *   Copyright (C) Cambridge Silicon Radio Ltd 2008. All rights reserved.
 *
 *   Refer to LICENSE.txt included with this source for details on the
 *   license terms.
 *
 * @section DESCRIPTION
 *   Implements Event Packing and Unpacking of signals for compatability
 *   across platforms. different Endianness and Native Packing schemes need
 *   to be supported.
 *
 *
 ****************************************************************************
 *
 * @section REVISION
 *   $Id: //depot/dot11/v6.1/host/lib_sme/common/event_pack_unpack/event_pack_unpack.h#1 $
 *
 ****************************************************************************/
#ifndef EVENT_PACK_UNPACK_H
#define EVENT_PACK_UNPACK_H

#ifdef __cplusplus
extern "C" {
#endif

/** @ingroup common
 * @{
 */

/* STANDARD INCLUDES ********************************************************/

/* PROJECT INCLUDES *********************************************************/
#include "abstractions/osa.h"
#include "hostio/hip_fsm_types.h"
#include "smeio/smeio_fsm_types.h"

/* PUBLIC MACROS ************************************************************/

/* PUBLIC TYPES DEFINITIONS *************************************************/

/* PUBLIC CONSTANT DECLARATIONS *********************************************/

/* PUBLIC VARIABLE DECLARATIONS *********************************************/

/* PUBLIC FUNCTION PROTOTYPES ***********************************************/

/**
 * @brief
 *   Packs data into the result buffer
 *
 * @par Description
 *   Packs data into a buffer from Native format into wire format.
 *
 * @return
 *    uint16 Number of bytes packed
 *
 */
extern uint16 event_pack_uint8 (uint8** resultBuffer, uint8  value);
extern uint16 event_pack_uint16(uint8** resultBuffer, uint16 value);
extern uint16 event_pack_uint32(uint8** resultBuffer, uint32 value);
#define event_pack_int8(resultBuffer, value)  event_pack_uint8(resultBuffer, (uint8)value)
#define event_pack_int16(resultBuffer, value) event_pack_uint16(resultBuffer, (uint16)value)
#define event_pack_int32(resultBuffer, value) event_pack_uint32(resultBuffer, (uint32)value)
extern uint16 event_pack_buffer(uint8** resultBuffer, uint8* value, uint16 numberOfBytes);
extern uint16 event_pack_string(uint8** resultBuffer, unifi_String value);

extern uint16 event_pack_hip_header(uint8** signalBuffer, uint16 id, uint16 pid, const DataReference* dataref1, const DataReference* dataref2);


extern uint8  event_unpack_uint8 (uint8** signalBuffer);
extern uint16 event_unpack_uint16(uint8** signalBuffer);
extern uint32 event_unpack_uint32(uint8** signalBuffer);
#define event_unpack_int8(signalBuffer)  (int8)event_unpack_uint8(signalBuffer)
#define event_unpack_int16(signalBuffer) (int16)event_unpack_uint16(signalBuffer)
#define event_unpack_int32(signalBuffer) (int32)event_unpack_uint32(signalBuffer)
extern void   event_unpack_buffer(uint8** signalBuffer, uint8* resultBuffer, uint16 numberOfBytes);
extern unifi_String event_unpack_string(uint8** signalBuffer);

/** @}
 */

#ifdef __cplusplus
}
#endif
#endif /* EVENT_PACK_UNPACK_H */
