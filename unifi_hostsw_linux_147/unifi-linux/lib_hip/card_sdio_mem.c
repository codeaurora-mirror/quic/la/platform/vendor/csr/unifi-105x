/*
 * ---------------------------------------------------------------------------
 * FILE: card_sdio_mem.c
 *
 * PURPOSE: Implementation of the Card API for SDIO. 
 *
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include "driver/unifi.h"
#include "card.h"

#define SDIO_RETRIES    3


#define retryable_sdio_error(_r) (((_r) == -EIO) || ((_r) == -ETIMEDOUT))


/*
 * ---------------------------------------------------------------------------
 *  retrying_readb
 *  retrying_writeb
 *
 *      These functions provide the first level of retry for SDIO operations.
 *      If an SDIO command fails for reason of a response timeout or CRC
 *      error, it is retried immediately. If three attempts fail we report a
 *      failure.
 *      If the command failed for any other reason, the failure is reported
 *      immediately.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      funcnum         The SDIO function to access.
 *                      Function 0 is the Card Configuration Register space,
 *                      function 1/2 is the UniFi register space.
 *      addr            Address to access
 *      pdata           Pointer in which to return the value read.
 *      data            Value to write.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
static INLINE int
retrying_readb(card_t *card, int funcnum,
               unsigned long addr, unsigned char *pdata)
{
    void *sdio = card->sdio_if;
    int r = 0;
    int retries;

    retries = 0;
    while (retries++ < SDIO_RETRIES) {
        r = unifi_sdio_readb(sdio, funcnum, addr, pdata);
        if (r == -ENODEV) {
            return r;
        }
        /* 
         * Try again for retryable (CRC or TIMEOUT) errors, 
         * break on success or fatal error
         */
        if (!retryable_sdio_error(r)) {
            break;
        }
        unifi_trace(card->ospriv, UDBG2, "retryable SDIO error reading F%d 0x%lX\n", funcnum, addr);
    }

    if ((r == 0) && (retries > 1)) {
        unifi_warning(card->ospriv, "Read succeeded after %d attempts\n", retries);
    }

    if (r) {
        unifi_error(card->ospriv, "Failed to read from UniFi (addr 0x%lX) after %d tries\n",
                    addr, retries - 1);
        /* Report any SDIO error as a general i/o error */
        r = -EIO;
    }

    return r;
} /* retrying_readb() */


static INLINE int
retrying_writeb(card_t *card, int funcnum,
                unsigned long addr, unsigned char data)
{
    void *sdio = card->sdio_if;
    int r = 0;
    int retries;

    retries = 0;
    while (retries++ < SDIO_RETRIES) {
        r = unifi_sdio_writeb(sdio, funcnum, addr, data);
        if (r == -ENODEV) {
            return r;
        }
        /* 
         * Try again for retryable (CRC or TIMEOUT) errors, 
         * break on success or fatal error
         */
        if (!retryable_sdio_error(r)) {
            break;
        }
        unifi_trace(card->ospriv, UDBG2, "retryable SDIO error writing %02X to F%d 0x%lX\n",
                    data, funcnum, addr);
    }

    if ((r == 0) && (retries > 1)) {
        unifi_warning(card->ospriv, "Write succeeded after %d attempts\n", retries);
    }

    if (r) {
        unifi_error(card->ospriv, "Failed to write to UniFi (addr 0x%lX) after %d tries\n",
                    addr, retries - 1);
        /* Report any SDIO error as a general i/o error */
        r = -EIO;
    }

    return r;
} /* retrying_writeb() */




/*
 * ---------------------------------------------------------------------------
 *  sdio_read_f0
 *
 *      Reads a byte value from the CCCR (func 0) area of UniFi.
 * 
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to read from
 *      pdata   Pointer in which to store the read value.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
int
sdio_read_f0(card_t *card, unsigned long addr, uint8 *pdata)
{
    return retrying_readb(card, 0, addr, pdata);
} /* sdio_read_f0() */


/*
 * ---------------------------------------------------------------------------
 *  sdio_write_f0
 *
 *      Writes a byte value to the CCCR (func 0) area of UniFi.
 * 
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to read from
 *      data    Data value to write.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
int
sdio_write_f0(card_t *card, unsigned long addr, uint8 data)
{
    return retrying_writeb(card, 0, addr, data);
} /* sdio_write_f0() */



/*
 * ---------------------------------------------------------------------------
 * unifi_read_direct8
 *
 *      Read a 8-bit value from the UniFi SDIO interface.
 *
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to read from
 *      pdata   Pointer in which to return data.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
int
unifi_read_direct8(card_t *card, unsigned long addr, uint8 *pdata)
{
    int r;
    unsigned char b;

    r = retrying_readb(card, card->function, addr, &b);
    if (r) {
        return r;
    }

    *pdata = b;

    return 0;
} /* unifi_read_direct8() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_write_direct8
 *
 *      Write a byte value to the UniFi SDIO interface.
 * 
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to write to
 *      data    Value to write.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      The even address *must* be written second. This is because writes to
 *      odd bytes are cached and not committed to memory until the preceding
 *      even address is written.
 * ---------------------------------------------------------------------------
 */
int
unifi_write_direct8(card_t *card, unsigned long addr, uint8 data)
{
    if (addr & 1) {
        unifi_warning(card->ospriv, "Warning: Byte write to an odd address (0x%lX) is dangerous\n",
                      addr);
    }
    return retrying_writeb(card, card->function, addr, data);
} /* unifi_write_direct8() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_read_direct16
 *
 *      Read a 16-bit value from the UniFi SDIO interface.
 *
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to read from
 *      pdata   Pointer in which to return data.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      The even address *must* be read first. This is because reads from
 *      odd bytes are cached and read from memory when the preceding
 *      even address is read.
 * ---------------------------------------------------------------------------
 */
int
unifi_read_direct16(card_t *card, unsigned long addr, uint16 *pdata)
{
    int r;
    unsigned char b0, b1;

    r = retrying_readb(card, card->function, addr, &b0);
    if (r) {
        return r;
    }

    r = retrying_readb(card, card->function, addr+1, &b1);
    if (r) {
        return r;
    }

    *pdata = ((uint16)b1 << 8) | b0;

    return 0;
} /* unifi_read_direct16() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_write_direct16
 *
 *      Write a 16-bit value to the UniFi SDIO interface.
 * 
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to write to
 *      data    Value to write.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      The even address *must* be written second. This is because writes to
 *      odd bytes are cached and not committed to memory until the preceding
 *      even address is written.
 * ---------------------------------------------------------------------------
 */
int
unifi_write_direct16(card_t *card, unsigned long addr, uint16 data)
{
    int r;
    unsigned char b0, b1;

    b1 = (data >> 8) & 0xFF;
    r = retrying_writeb(card, card->function, addr+1, b1);
    if (r) {
        return r;
    }

    b0 = data & 0xFF;
    r = retrying_writeb(card, card->function, addr, b0);
    if (r) {
        return r;
    }

    return 0;
} /* unifi_write_direct16() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_read_direct32
 *
 *      Read a 32-bit value from the UniFi SDIO interface.
 *
 *  Arguments:
 *      card    Pointer to card structure.
 *      addr    Address to read from
 *      pdata   Pointer in which to return data.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
int
unifi_read_direct32(card_t *card, unsigned long addr, uint32 *pdata)
{
    int r;
    unsigned char b0, b1, b2, b3;

    r = retrying_readb(card, card->function, addr, &b0);
    if (r) {
        return r;
    }

    r = retrying_readb(card, card->function, addr+1, &b1);
    if (r) {
        return r;
    }

    r = retrying_readb(card, card->function, addr+2, &b2);
    if (r) {
        return r;
    }

    r = retrying_readb(card, card->function, addr+3, &b3);
    if (r) {
        return r;
    }

    *pdata = ((uint32)b3 << 24) | ((uint32)b2 << 16) | ((uint32)b1 << 8) | b0;

    return 0;
} /* unifi_read_direct32() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_read_directn_match
 *
 *      Read multiple 8-bit values from the UniFi SDIO interface,
 *      stopping when either we have read 'len' bytes or we have read
 *      a octet equal to 'match'.  If 'match' is not a valid octet
 *      then this function is the same as 'unifi_read_directn'.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      addr            Start address to read from.
 *      pdata           Pointer to which to write data.
 *      len             Maximum umber of bytes to read
 *      match           The value to stop reading at.
 *
 *  Returns:
 *      number of octets read on success, negative error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      The even address *must* be read first. This is because reads from
 *      odd bytes are cached and read from memory when the preceding
 *      even address is read.
 * ---------------------------------------------------------------------------
 */
static int
unifi_read_directn_match(card_t *card, unsigned long addr, void *pdata, int len, int m)
{
    int r, i;
    unsigned char b;
    unsigned char *cptr;

    cptr = (unsigned char *)pdata;
    for (i=0; i<len; i++) {
        r = retrying_readb(card, card->function, addr, &b);
        if (r) {
            return r;
        }

        *cptr++ = b;
        addr++;

        if ((m >= 0) && ((int)b == m)) {
            break;
        }
    }

    return cptr - (unsigned char *)pdata;
}


/*
 * ---------------------------------------------------------------------------
 *  unifi_read_directn
 *
 *      Read multiple 8-bit values from the UniFi SDIO interface.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      addr            Start address to read from.
 *      pdata           Pointer to which to write data.
 *      len             Number of bytes to read
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      The even address *must* be read first. This is because reads from
 *      odd bytes are cached and read from memory when the preceding
 *      even address is read.
 * ---------------------------------------------------------------------------
 */
int
unifi_read_directn(card_t *card, unsigned long addr, void *pdata, int len)
{
    int r;
    r = unifi_read_directn_match(card, addr, pdata, len, -1);
    return (r < 0) ? r : 0;
} /* unifi_read_directn() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_write_directn
 *
 *      Write multiple 8-bit values to the UniFi SDIO interface.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      addr            Start address to write to.
 *      pdata           Source data pointer.
 *      len             Number of bytes to write, must be even.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      The UniFi has a peculiar 16-bit bus architecture. Writes are only
 *      committed to memory when an even address is accessed. Writes to
 *      odd addresses are cached and only committed if the next write is
 *      to the preceding address.
 *      This means we must write data as pairs of bytes in reverse order.
 * ---------------------------------------------------------------------------
 */
int
unifi_write_directn(card_t *card, unsigned long addr, void *pdata, int len)
{
    int r;
    unsigned char b0, b1;
    unsigned char *cptr;

    cptr = (unsigned char *)pdata;
    while (len) {
        b0 = *cptr++;
        b1 = *cptr++;

        r = retrying_writeb(card, card->function, addr+1, b1);
        if (r) {
            return r;
        }

        r = retrying_writeb(card, card->function, addr, b0);
        if (r) {
            return r;
        }

        addr += 2;
        len -= 2;
    }

    return 0;
} /* unifi_write_directn() */



/*
 * ---------------------------------------------------------------------------
 *  set_dmem_page
 *  set_pmem_page
 *
 *      Set up the page register for the shared data memory window or program
 *      memory window.
 *  
 *  Arguments:
 *      card            Pointer to card structure.
 *      dmem_addr       UniFi shared-data-memory address to access.
 *      pmem_addr       UniFi program memory address to access. This includes
 *                        External FLASH memory at    0x000000
 *                        Processor program memory at 0x200000
 *                        External SRAM at memory     0x400000
 *
 *  Returns:
 *      Returns a SDIO address (24-bit) for use in a unifi_read_direct or
 *      unifi_write_direct call, or a negative error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
static int
set_dmem_page(card_t *card, unsigned long dmem_addr)
{
    uint16 page, addr;
    uint32 len;
    int r;

    if (!ChipHelper_DecodeWindow(card->helper,
                                 CHIP_HELPER_WINDOW_3,
                                 CHIP_HELPER_WT_SHARED,
                                 dmem_addr / 2,
                                 &page, &addr, &len)) {
        unifi_error(card->ospriv, "Failed to decode SHARED_DMEM_PAGE %08lx\n", dmem_addr);
        return -EINVAL;
    }

    if (page != card->dmem_page) {
        unifi_trace(card->ospriv, UDBG6, "setting dmem page=0x%X, addr=0x%lX\n", page, addr);

        /* change page register */
        r = unifi_write_direct16(card, ChipHelper_HOST_WINDOW3_PAGE(card->helper) * 2, page);
        if (r) {
            unifi_error(card->ospriv, "Failed to write SHARED_DMEM_PAGE\n");
            return r;
        }

        card->dmem_page = page;
    }

    return ((unsigned int)addr * 2) + (dmem_addr & 1);
} /* set_dmem_page() */



static int
set_pmem_page(card_t *card, unsigned long pmem_addr,
              enum chip_helper_window_type mem_type)
{
    uint16 page, addr;
    uint32 len;
    int r;

    if (!ChipHelper_DecodeWindow(card->helper,
                                 CHIP_HELPER_WINDOW_2,
                                 mem_type,
                                 pmem_addr / 2,
                                 &page, &addr, &len)) {
        unifi_error(card->ospriv, "Failed to decode PROG MEM PAGE %08lx %d\n", pmem_addr, mem_type);
        return -EINVAL;
    }

    if (page != card->pmem_page) {
        unifi_trace(card->ospriv, UDBG6, "setting pmem page=0x%X, addr=0x%lX\n", page, addr);

        /* change page register */
        r = unifi_write_direct16(card, ChipHelper_HOST_WINDOW2_PAGE(card->helper) * 2, page);
        if (r) {
            unifi_error(card->ospriv, "Failed to write PROG MEM PAGE\n");
            return r;
        }

        card->pmem_page = page;
    }

    return ((unsigned int)addr * 2) + (pmem_addr & 1);
} /* set_pmem_page() */




/*
 * ---------------------------------------------------------------------------
 *  set_page
 *
 *      Sets up the appropriate page register to access the given address.
 *      Returns the sdio address at which the unifi address can be accessed.
 *
 *  Arguments:
 *      card            Pointer to card structure.
 *      generic_addr    UniFi internal address to access, in Generic Pointer
 *                      format, i.e. top byte is space indicator.
 *
 *  Returns:
 *      Returns a SDIO address (24-bit) for use in a unifi_read_direct or
 *      unifi_write_direct call, or a negative error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
static int
set_page(card_t *card, unsigned long generic_addr)
{
    int space;
    unsigned long addr;
    int sdio_addr;
    int r;

    space = UNIFI_GP_SPACE(generic_addr);
    addr = UNIFI_GP_OFFSET(generic_addr);
    switch (space)
    {
    case UNIFI_SH_DMEM:
        /* Shared Data Memory is accessed via the Shared Data Memory window */
        sdio_addr = set_dmem_page(card, addr);
        break;

    case UNIFI_EXT_FLASH:
        if (!ChipHelper_HasFlash(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        /* External FLASH is accessed via the Program Memory window */
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_FLASH);
        break;

    case UNIFI_EXT_SRAM:
        if (!ChipHelper_HasExtSram(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        /* External SRAM is accessed via the Program Memory window */
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_EXT_SRAM);
        break;

    case UNIFI_REGISTERS:
        /* Registers are accessed directly */
        sdio_addr = addr;
        break;

    case UNIFI_PHY_DMEM:
        r = unifi_set_proc_select(card, UNIFI_PROC_PHY);
        if (r)
            return r;
        sdio_addr = ChipHelper_DATA_MEMORY_RAM_OFFSET(card->helper) * 2 + addr;
        break;

    case UNIFI_MAC_DMEM:
        r = unifi_set_proc_select(card, UNIFI_PROC_MAC);
        if (r)
            return r;
        sdio_addr = ChipHelper_DATA_MEMORY_RAM_OFFSET(card->helper) * 2 + addr;
        break;

    case UNIFI_BT_DMEM:
        if (!ChipHelper_HasBt(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        r = unifi_set_proc_select(card, UNIFI_PROC_BT);
        if (r)
            return r;
        sdio_addr = ChipHelper_DATA_MEMORY_RAM_OFFSET(card->helper) * 2 + addr;
        break;

    case UNIFI_PHY_PMEM:
        r = unifi_set_proc_select(card, UNIFI_PROC_PHY);
        if (r)
            return r;
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_CODE_RAM);
        break;

    case UNIFI_MAC_PMEM:
        r = unifi_set_proc_select(card, UNIFI_PROC_MAC);
        if (r)
            return r;
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_CODE_RAM);
        break;

    case UNIFI_BT_PMEM:
        if (!ChipHelper_HasBt(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        r = unifi_set_proc_select(card, UNIFI_PROC_BT);
        if (r)
            return r;
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_CODE_RAM);
        break;

    case UNIFI_PHY_ROM:
        if (!ChipHelper_HasRom(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        r = unifi_set_proc_select(card, UNIFI_PROC_PHY);
        if (r)
            return r;
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_ROM);
        break;

    case UNIFI_MAC_ROM:
        if (!ChipHelper_HasRom(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        r = unifi_set_proc_select(card, UNIFI_PROC_MAC);
        if (r)
            return r;
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_ROM);
        break;

    case UNIFI_BT_ROM:
        if (!ChipHelper_HasRom(card->helper) || !ChipHelper_HasBt(card->helper)) {
            unifi_error(card->ospriv, "Bad address space for chip in generic pointer 0x%08lX\n",
                        generic_addr);
            return -EINVAL;
        }
        r = unifi_set_proc_select(card, UNIFI_PROC_BT);
        if (r)
            return r;
        sdio_addr = set_pmem_page(card, addr, CHIP_HELPER_WT_ROM);
        break;

    default:
        unifi_error(card->ospriv, "Bad address space in generic pointer 0x%08lX\n",
                    generic_addr);
        return -EINVAL;
    }

    return sdio_addr;

} /* set_page() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_set_proc_select
 *
 *
 *  Arguments:
 *      card            Pointer to card structure.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 * ---------------------------------------------------------------------------
 */
int
unifi_set_proc_select(card_t *card, int select)
{
    int r;

    /* Verify the the select value is allowed. */
    switch (select)
    {
    case UNIFI_PROC_MAC:
    case UNIFI_PROC_PHY:
    case UNIFI_PROC_BOTH:
        break;


    default:
        return -EINVAL;
    }

    if (card->proc_select != select) {

        r = unifi_write_direct8(card, ChipHelper_DBG_HOST_PROC_SELECT(card->helper) * 2, select);
        if (r == -ENODEV) return r;
        if (r) {
            unifi_error(card->ospriv, "Failed to write to Proc Select register\n");
            return r;
        }

        card->proc_select = select;
    }

    return 0;
}


/*
 * ---------------------------------------------------------------------------
 *  unifi_read8
 *
 *      Performs a byte read of the given address in shared data memory.
 *      Set up the shared data memory page register as required.
 *
 *  Arguments:
 *      card            Pointer to card structure.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      pdata           Pointer to a byte variable for the value read.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL  a bad generic pointer was specified
 * ---------------------------------------------------------------------------
 */
int
unifi_read8(card_t *card, unsigned long unifi_addr, uint8 *pdata)
{
    int sdio_addr;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    return unifi_read_direct8(card, sdio_addr, pdata);
} /* unifi_read8() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_write8
 *
 *      Performs a byte write of the given address in shared data memory.
 *      Set up the shared data memory page register as required.
 * 
 *  Arguments:
 *      card            Pointer to card context struct.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      data            Value to write.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL  a bad generic pointer was specified
 *
 *  Notes:
 *      Beware using unifi_write8() because byte writes are not safe on UniFi.
 *      Writes to odd bytes are cached, writes to even bytes perform a 16-bit
 *      write with the previously cached odd byte.
 * ---------------------------------------------------------------------------
 */
int
unifi_write8(card_t *card, unsigned long unifi_addr, uint8 data)
{
    int sdio_addr;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    return unifi_write_direct8(card, sdio_addr, data);
} /*  unifi_write8() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_read16
 *
 *      Performs a 16-bit read of the given address in shared data memory.
 *      Set up the shared data memory page register as required.
 *
 *  Arguments:
 *      card            Pointer to card structure.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      pdata           Pointer to a 16-bit int variable for the value read.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL  a bad generic pointer was specified
 * ---------------------------------------------------------------------------
 */
int
unifi_read16(card_t *card, unsigned long unifi_addr, uint16 *pdata)
{
    int sdio_addr;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    return unifi_read_direct16(card, sdio_addr, pdata);
} /* unifi_read16() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_write16
 *
 *      Performs a 16-bit write of the given address in shared data memory.
 *      Set up the shared data memory page register as required.
 *
 *  Arguments:
 *      card            Pointer to card structure.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      pdata           Pointer to a byte variable for the value write.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL  a bad generic pointer was specified
 * ---------------------------------------------------------------------------
 */
int
unifi_write16(card_t *card, unsigned long unifi_addr, uint16 data)
{
    int sdio_addr;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    return unifi_write_direct16(card, sdio_addr, data);
} /* unifi_write16() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_read32
 *
 *      Performs a 32-bit read of the given address in shared data memory.
 *      Set up the shared data memory page register as required.
 *
 *  Arguments:
 *      card            Pointer to card structure.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      pdata           Pointer to a int variable for the value read.
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL  a bad generic pointer was specified
 * ---------------------------------------------------------------------------
 */
int
unifi_read32(card_t *card, unsigned long unifi_addr, uint32 *pdata)
{
    int sdio_addr;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    return unifi_read_direct32(card, sdio_addr, pdata);
} /* unifi_read32() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_readn
 *  unifi_readnz
 *
 *      Read multiple 8-bit values from the UniFi SDIO interface.
 *      This function interprets the address as a GenericPointer as
 *      defined in the UniFi Host Interface Protocol Specification.
 *      The readnz version of this function will stop when it reads a
 *      zero octet.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      pdata           Pointer to which to write data.
 *      len             Number of bytes to read
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL  a bad generic pointer was specified
 * ---------------------------------------------------------------------------
 */
int
unifi_readn_match(card_t *card, unsigned long unifi_addr, void *pdata, int len, int match)
{
    int sdio_addr, r;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    r = unifi_read_directn_match(card, sdio_addr, pdata, len, match);
    return (r < 0) ? r : 0;
} /* unifi_readn_match() */

int
unifi_readn(card_t *card, unsigned long unifi_addr, void *pdata, int len)
{
    return unifi_readn_match(card, unifi_addr, pdata, len, -1);
} /* unifi_readn() */

int
unifi_readnz(card_t *card, unsigned long unifi_addr, void *pdata, int len)
{
    return unifi_readn_match(card, unifi_addr, pdata, len, 0);
} /* unifi_readnz() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_writen
 *
 *      Write multiple 8-bit values to the UniFi SDIO interface using CMD52
 *      This function interprets the address as a GenericPointer as
 *      defined in the UniFi Host Interface Protocol Specification.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      unifi_addr      UniFi shared-data-memory address to access.
 *      pdata           Pointer to which to write data.
 *      len             Number of bytes to write
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *         -EINVAL    an odd length or length too big.
 *         -ETIMEDOUT timed out waiting for rewind.
 * ---------------------------------------------------------------------------
 */
int
unifi_writen(card_t *card, unsigned long unifi_addr, void *pdata, int len)
{
    int sdio_addr;

    sdio_addr = set_page(card, unifi_addr);
    if (sdio_addr < 0) {
        return sdio_addr;
    }

    return unifi_write_directn(card, sdio_addr, pdata, len);
} /* unifi_writen() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_bulk_rw
 *
 *      Transfer bulk data to or from the UniFi SDIO interface.
 *      This function is used to read or write signals and bulk data.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      handle          Value to put in the Register Address field of the CMD53 req.
 *      data            Pointer to data to write.
 *      direction       One of UNIFI_SDIO_READ or UNIFI_SDIO_WRITE
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      This function uses SDIO CMD53, which is the block transfer mode.
 * ---------------------------------------------------------------------------
 */
int
unifi_bulk_rw(card_t *card, unsigned long handle, void *pdata,
              unsigned int len, int direction)
{
#define CMD53_RETRIES 3
/* The spec gives us no hint as to what these should be, I've seen
 * 50,10 time out, so I've upped them.  I can see no real problem with
 * this, if a better mechanism is required then we should trigger off
 * the interrupt generated by UniFi when the rewind handle is reset. */
#define REWIND_RETRIES  250 /*  5, 50 */
#define REWIND_DELAY     20 /* 10, 10 */
    int r = 0;
    int retries = CMD53_RETRIES;
    int stat_retries;
    uint8 stat;
#ifdef MAKE_FAKE_CMD53_ERRORS
    static int fake_error;
#endif
    int dump_read;

    dump_read = 0;
    if (((unsigned long)pdata) & 1) {
        unifi_notice(card->ospriv, "CD53 request on a unaligned buffer (addr: 0x%X) dir %s-Host\n",
                     pdata, (direction == UNIFI_SDIO_READ) ? "To" : "From");
        if (direction == UNIFI_SDIO_WRITE) {
            dump(pdata, len);
        } else {
            dump_read = 1;
        }
    }

    /* Defensive check */
    if ((len & 1) || (len > 0xffff)) {
        unifi_error(card->ospriv, "Impossible CMD53 length requested: %d\n", len);
        return -EINVAL;
    }

    while (1)
    {
        r = unifi_sdio_block_rw(card->sdio_if, card->function, handle,
                                (unsigned char *)pdata, len, direction);
        if (r == -ENODEV) {
            return r;
        }
#ifdef MAKE_FAKE_CMD53_ERRORS
        if (++fake_error > 100) {
            fake_error = 90;
            unifi_warning(card->ospriv, "Faking a CMD53 error,\n");
            if (r == 0) {
                r = -EIO;
            }
        }
#endif
        if (r == 0) {
            /* success */
            if (dump_read) {
                dump(pdata, len);
            }
            break;
        }

        /*
         * At this point the SDIO driver should have written the I/O Abort
         * register to notify UniFi that the command has failed.
         * UniFi-1 and UniFi-2 use the same register to store the 
         * Deep Sleep State. This means we have to restore the Deep Sleep
         * State (AWAKE in any case since we can not perform a CD53 in any other
         * state) by rewritting the I/O Abort register to its' previous value.
         */
        unifi_set_host_state(card, UNIFI_HOST_STATE_AWAKE);

        if (!retryable_sdio_error(r)) {
            unifi_error(card->ospriv, "Fatal error in a CMD53 transfer\n");
            break;
        }

        /*
         * These happen from time to time, life sucks, try again
         */
        unifi_notice(card->ospriv, "Error in a CMD53 transfer, retrying (h:%d,l:%u)...\n",
                     (int)handle & 0xff, len);

        if (--retries == 0) {
            break;
        }

        /* The transfer failed, rewind and try again */
        r = unifi_write8(card, card->sdio_ctrl_addr+8, (uint8)(handle & 0xff));
        if (r == -ENODEV) {
            return r;
        }
        if (r) {
            /* 
             * If we can't even do CMD52 (register read/write) then
             * stop here.
             */
            unifi_error(card->ospriv, "Failed to write REWIND cmd\n");
            return r;
        }

        /* Signal the UniFi to look for the rewind request. */
        r = CardGenInt(card);
        if (r) {
            return r;
        }

        /* Wait for UniFi to acknowledge the rewind */
        stat_retries = REWIND_RETRIES;
        while (1) {
            r = unifi_read8(card, card->sdio_ctrl_addr+8, &stat);
            if (r == -ENODEV) {
                return r;
            }
            if (r) {
                unifi_error(card->ospriv, "Failed to read REWIND status\n");
                return -EIO;
            }
            if (stat == 0) {
                break;
            }
            if (--stat_retries == 0) {
                unifi_error(card->ospriv, "Timeout waiting for REWIND ready\n");
                return -ETIMEDOUT;
            }
            unifi_delay_us(card->ospriv, REWIND_DELAY);
        }

    }

    if (r) {
        unifi_error(card->ospriv, "Block %s failed after %d retries\n",
                    (direction == UNIFI_SDIO_READ) ? "read" : "write",
                    CMD53_RETRIES - retries);
        /* Report any SDIO error as a general i/o error */
        return -EIO;
    }

    /* Collect some stats */
    if (direction == UNIFI_SDIO_READ) {
        card->sdio_bytes_read += len;
    } else {
        card->sdio_bytes_written += len;
    }

    return 0;
} /* unifi_bulk_rw() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_bulk_rw
 *
 *      Transfer bulk data to or from the UniFi SDIO interface.
 *      This function is used to read or write signals and bulk data.
 * 
 *  Arguments:
 *      card            Pointer to card structure.
 *      handle          Value to put in the Register Address field of
 *                      the CMD53 req.
 *      data            Pointer to data to write.
 *      direction       One of UNIFI_SDIO_READ or UNIFI_SDIO_WRITE
 *
 *  Returns:
 *      0 on success, non-zero error code on error:
 *         -ENODEV  card was ejected
 *         -EIO     an SDIO error occurred
 *
 *  Notes:
 *      This function uses SDIO CMD53, which is the block transfer mode.
 * ---------------------------------------------------------------------------
 */
int
unifi_bulk_rw_noretry(card_t *card, unsigned long handle, void *pdata,
                      unsigned int len, int direction)
{
    int r;

    r = unifi_sdio_block_rw(card->sdio_if, card->function, handle,
                            (unsigned char *)pdata, len, direction);
    if (r == -ENODEV) {
        return r;
    }
    if (r) {
        unifi_error(card->ospriv, "Block %s failed\n",
                    (direction == UNIFI_SDIO_READ) ? "read" : "write");
        /* Report any SDIO error as a general i/o error */
        r = -EIO;
    }

    return r;
} /* unifi_bulk_rw() */
