/*
 ******************************************************************************
 * FILE : card.h
 *
 * PURPOSE : Defines abstract interface for hardware specific functions.
 *           Note, this is a different file from one of the same name in the
 *           Windows driver.
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *****************************************************************************
 */
#ifndef __CARD_H__
#define __CARD_H__


#include "card_sdio.h"
#include "driver/signals.h"
#include "driver/unifi_udi.h"



/*****************************************************************************
 * CardEnableInt -
 */
int CardEnableInt(card_t *card);

/*****************************************************************************
 * CardGenInt -
 */
int CardGenInt(card_t *card);

/*****************************************************************************
 * CardPendingInt -
 */
int CardPendingInt(card_t *card);

/*****************************************************************************
 * CardDisableInt -
 */
int CardDisableInt(card_t *card);

/*****************************************************************************
 * CardClearInt -
 */
int CardClearInt(card_t *card);

/*****************************************************************************
 * CardDisable -
 */
void CardDisable(card_t *card);

/*****************************************************************************
 * CardIntEnabled -
 */
int CardIntEnabled(card_t *card);

/*****************************************************************************
 * CardGetDataSlotSize
 */
int CardGetDataSlotSize(card_t *card);

/*****************************************************************************
 * CardWriteBulkData -
 */
int CardWriteBulkData(card_t *card, bulk_data_desc_t* bulkdata);

/*****************************************************************************
 * CardClearFromHostDataSlot -
 */
void CardClearFromHostDataSlot(card_t *card, const int aSlotNum);

/*****************************************************************************
 * CardGetFromHostDataSlot -
 *   returns -1 if no slots available
 */
int CardGetFromHostDataSlot(card_t *card, unsigned int size);

/*****************************************************************************
 * CardGetFreeFromHostDataSlots -
 */
unsigned int CardGetFreeFromHostDataSlots(card_t *card);


int card_stop_processor(card_t *card, int which);
int card_start_processor(card_t *card, int which);

int card_wait_for_firmware_to_start(card_t *card, unsigned long *paddr);

int unifi_dl_firmware(card_t *card, void *arg, int secondary);
int unifi_dl_patch(card_t *card, void *arg, unsigned long boot_ctrl);
int unifi_do_loader_op(card_t *card, unsigned long op_addr, unsigned char opcode);


#endif /* __CARD_H__ */
