/*
 * Operating system kernel abstraction -- misc. utility functions
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_UTIL_H
#define __OSKA_UTIL_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup util Miscellaneous utilties
 */

/**
 * Return a CPU (native) byte ordered 16-bit word in little endian
 * byte order.
 *
 * @param x CPU ordered word.
 *
 * @return the word in little-endian byte order.
 *
 * @ingroup util
 */
uint16_t os_cpu_to_le16(uint16_t x);

/**
 * Return a little endian byte ordered 16-bit word in CPU (native)
 * byte order.
 *
 * @param x little endian word.
 *
 * @return the word in native byte order.
 *
 * @ingroup util
 */
uint16_t os_le16_to_cpu(uint16_t x);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/util.h>
#else
#  error <oska/util.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_UTIL_H */
