/*
 * OSKA Linux implementation -- misc. utility functions
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_LINUX_UTILS_H
#define __OSKA_LINUX_UTILS_H

#include <linux/kernel.h>
#include <asm/byteorder.h>

static inline uint16_t os_le16_to_cpu(uint16_t x)
{
    return le16_to_cpu(x);
}

static inline uint16_t os_cpu_to_le16(uint16_t x)
{
    return cpu_to_le16(x);
}

#endif /* __OSKA_LINUX_UTILS_H */
