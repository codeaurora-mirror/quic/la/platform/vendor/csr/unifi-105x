/*
 * SDIO Bluetooth Type-A interface definitions.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef SDIOEMB_SDIO_BT_A_H
#define SDIOEMB_SDIO_BT_A_H

#include <sdioemb/sdio_csr.h>

/*
 * Standard SDIO function registers for a Bluetooth Type-A interface.
 */
#define SDIO_BT_A_RD 0x00
#define SDIO_BT_A_TD 0x00

#define SDIO_BT_A_RX_PKT_CTRL 0x10
#  define PC_RRT 0x01

#define SDIO_BT_A_TX_PKT_CTRL 0x11
#  define PC_WRT 0x01

#define SDIO_BT_A_RETRY_CTRL  0x12
#  define RTC_STAT 0x01
#  define RTC_SET  0x01

#define SDIO_BT_A_INTRD       0x13
#  define INTRD 0x01
#  define CL_INTRD 0x01

#define SDIO_BT_A_INT_EN      0x14
#  define EN_INTRD 0x01

#define SDIO_BT_A_BT_MODE     0x20
#  define MD_STAT 0x01

/*
 * Length of the Type-A header.
 *
 * Packet length (3 octets) plus Service ID (1 octet).
 */
#define SDIO_BT_A_HEADER_LEN 4

/*
 * Maximum length of a Type-A transport packet.
 *
 * Type-A header length and maximum length of a HCI packet (65535
 * octets).
 */
#define SDIO_BT_A_PACKET_LEN_MAX 65543

enum sdio_bt_a_service_id {
    SDIO_BT_A_SID_CMD    = 0x01,
    SDIO_BT_A_SID_ACL    = 0x02,
    SDIO_BT_A_SID_SCO    = 0x03,
    SDIO_BT_A_SID_EVT    = 0x04,
    SDIO_BT_A_SID_VENDOR = 0xfe,
};

static inline int sdio_bt_a_packet_len(const char *p)
{
    return (p[0] & 0xff) | ((p[1] & 0xff) << 8) | ((p[2] & 0xff) << 16);
}

static inline int sdio_bt_a_service_id(const char *p)
{
    return p[3];
}

struct sdio_bt_a_pkt_info {
    void *  data;
    int     data_len; /* excluding Type-A header */
    uint8_t hdr[SDIO_BT_A_HEADER_LEN];
    uint8_t tx_data[0];
};

/*
 * Minimum amount to read (excluding the Type-A header). This allows
 * short packets (e.g., flow control packets) to be read with a single
 * command.
 */
#define SDIO_BT_A_MIN_READ (32 - SDIO_BT_A_HEADER_LEN)

#define SDIO_BT_A_NAME_LEN 16

struct sdio_bt_a_dev {
    struct sdio_dev *fdev;
    char name[SDIO_BT_A_NAME_LEN];
    void *drv_data;

    void (*receive)(struct sdio_bt_a_dev *bt, struct sdio_bt_a_pkt_info *pkt);
    void (*sleep_state_changed)(struct sdio_bt_a_dev *bt);

    enum sdio_sleep_state sleep_state;

    uint8_t  max_tx_retries;
    uint8_t  max_rx_retries;
    unsigned needs_read_ack:1;
    unsigned wait_for_firmware:1;
};

void sdio_bt_a_init(struct sdio_bt_a_dev *bt, struct sdio_dev *fdev);
int  sdio_bt_a_send(struct sdio_bt_a_dev *bt, const struct sdio_bt_a_pkt_info *pkt);
void sdio_bt_a_handle_interrupt(struct sdio_bt_a_dev *bt);
void sdio_bt_a_set_sleep_state(struct sdio_bt_a_dev *bt, enum sdio_sleep_state state);
int  sdio_bt_a_check_for_reset(struct sdio_bt_a_dev *bt);
void sdio_bt_a_start(struct sdio_bt_a_dev *bt);
void sdio_bt_a_stop(struct sdio_bt_a_dev *bt);

#endif /* #ifndef SDIOEMB_SDIO_BT_A_H */
