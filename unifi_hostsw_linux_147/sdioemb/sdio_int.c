/*
 * Card interrupt management.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include "sdio_layer.h"

/**
 * Allow this SDIO device to receive card interrupt signals.
 *
 * This only affects the routing of card interrupts from the core.
 *
 * Callable from: any context.
 *
 * @param fdev the SDIO device to enable interrupt routing for.
 * 
 * @note The core always sets the I/O Enable bits for all functions,
 * so interrupts sources on the card should be enabled and disabled in
 * a function specific manner by the function driver.
 *
 * @ingroup fdriver
 */
void sdio_enable_interrupt(struct sdio_dev *fdev)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    unsigned int_enabled;
    os_int_status_t istate;

    os_spinlock_lock_intsave(&slotp->lock, &istate);

    int_enabled = slotp->int_enabled | (1 << fdev->function);
    if (slotp->int_enabled == 0) {
        slot->enable_card_int(slot);
    }
    slotp->int_enabled = int_enabled;

    sdio_event_log(slot, SDD_EVENT_INT_UNMASKED, 1 << fdev->function);

    os_spinlock_unlock_intrestore(&slotp->lock, &istate);
}


/**
 * Prevent this SDIO device from receiving card interrupt signals.
 *
 * This only affects the routing of card interrupts from the core.
 *
 * Function drivers should ensure they clear any pending interrupts
 * before calling this.
 *
 * Callable from: any context.
 *
 * @param fdev the SDIO device to disable interrupt routing for.
 *
 * @see sdio_enable_interrupt().
 *
 * @ingroup fdriver
 */
void sdio_disable_interrupt(struct sdio_dev *fdev)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    unsigned int_enabled;
    os_int_status_t istate;

    os_spinlock_lock_intsave(&slotp->lock, &istate);

    int_enabled = slotp->int_enabled & ~(1 << fdev->function);
    if (int_enabled == 0) {
        slot->disable_card_int(slot);
    }
    slotp->int_enabled = int_enabled;

    sdio_event_log(slot, SDD_EVENT_INT_MASKED, 1 << fdev->function);

    os_spinlock_unlock_intrestore(&slotp->lock, &istate);
}

/**
 * Notify the SDIO layer that the card interrupt is asserted.
 *
 * Callable from: interrupt context.
 *
 * @param slot the slot with the asserted interrupt.
 *
 * @ingroup sdriver
 */
void sdio_interrupt(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int f;

    sdio_event_log(slot, SDD_EVENT_CARD_INT);

    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        struct sdio_dev *fdev = slotp->functions[f];
        if (fdev && slotp->int_enabled & (1 << f)) {
            fdev->driver->card_int_handler(fdev);
        }
    }
}
