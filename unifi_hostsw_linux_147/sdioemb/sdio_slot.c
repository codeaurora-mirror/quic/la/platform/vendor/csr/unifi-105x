/*
 * Slot driver management.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include "sdio_config.h"
#include "sdio_layer.h"

/*
 * A list of registered slots is kept so that: sdio_slot_unregister()
 * can be called with an unregistered slot; and the slot's position in
 * the slot list is the slot ID.
 */
struct sdio_slot *registered_slots[SDD_SLOTS_MAX];

/*
 * Add slot to registered slot list.
 */
static int slot_add(struct sdio_slot *slot)
{
    int i;

    os_mutex_lock(&sdio_core_mutex);
    for (i = 0; i < SDD_SLOTS_MAX; i++) {
        if (registered_slots[i] == NULL) {
            registered_slots[i] = slot;
            slot->priv->id = i;
            break;
        }
    }
    os_mutex_unlock(&sdio_core_mutex);

    if (i == SDD_SLOTS_MAX) {
        return -EINVAL;
    }
    return 0;
}

/*
 * Remove slot from registered slot list.
 */
static int slot_del(struct sdio_slot *slot)
{
    int i;

    os_mutex_lock(&sdio_core_mutex);
    for (i = 0; i < SDD_SLOTS_MAX; i++) {
        if (registered_slots[i] == slot) {
            registered_slots[i] = NULL;
            break;
        }
    }
    os_mutex_unlock(&sdio_core_mutex);

    if (i == SDD_SLOTS_MAX) {
        return -EINVAL;
    }
    return 0;
}

/**
 * Allocate and initialize and SDIO slot driver structure.
 *
 * Callable from: thread context.
 *
 * @param drv_data_size size of slot driver's drv_data.
 *
 * @return the allocated structure; or NULL if no memory could be
 * allocated.
 *
 * @ingroup sdriver
 */
struct sdio_slot *sdio_slot_alloc(size_t drv_data_size)
{
    struct sdio_slot *slot;
    struct sdio_slot_priv *slotp;

    slot = os_alloc(sizeof(struct sdio_slot)
                    + sizeof(struct sdio_slot_priv)
                    + drv_data_size);
    if (!slot)
        return NULL;

    slotp = slot->priv = (struct sdio_slot_priv *)&slot[1];
    slot->drv_data = (uint8_t *)slot->priv + sizeof(struct sdio_slot_priv);

    os_spinlock_init(&slotp->lock);
    os_mutex_init(&slotp->card_mutex);

    /* Sensible defaults for some capabilities. */
    slot->caps.max_bus_freq = SDIO_CLOCK_FREQ_NORMAL_SPD;

    return slot;
}

/**
 * Free a slot_driver allocated with sdio_slot_alloc().
 *
 * Callable from: thread context.
 *
 * @param slot the slot driver to free.
 *
 * @ingroup sdriver
 */
void sdio_slot_free(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    os_spinlock_destroy(&slotp->lock);
    os_free(slot);
}


/**
 * Register a slot driver with the SDIO layer.
 *
 * Callable from: thread context.
 *
 * @param slot the slot driver to register.
 *
 * @return 0 on success; -ve on error: -ENOMEM - maximum number of
 * slots are already registered.
 *
 * @see SDD_SLOTS_MAX
 *
 * @ingroup sdriver
 */
int sdio_slot_register(struct sdio_slot *slot)
{
    int ret;

    ret = slot_add(slot);
    if (ret < 0)
        return ret;

    sdio_event_log_init(slot);

    ret = sdio_card_detect_init(slot);
    if (ret) {
        slot_del(slot);
        sdio_event_log_deinit(slot);
        return ret;
    }

    slot_info(NULL, "registered slot %s", slot->name);

    return 0;
}

/**
 * Unregister a slot driver from the SDIO layer.
 *
 * Callable from: thread context.
 *
 * @param slot the slot driver to unregister.
 *
 * @ingroup sdriver
 */
void sdio_slot_unregister(struct sdio_slot *slot)
{
    if (slot_del(slot) < 0) {
        return;
    }

    slot_info(NULL, "unregister slot %s", slot->name);

    sdio_card_detect_exit(slot);
    sdio_event_log_deinit(slot);
}

/**
 * Start a command after setting the bus to the active frequency (if necessary).
 *
 * @param slot the slot to perform the command.
 * @param cmd  the command to start.
 *
 * @return 0 on success.
 * @return -EINVAL if the slot cannot handle the command.
 */
int slot_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct sdio_slot_priv *slotp = slot->priv;

    cmd->status = SDD_CMD_IN_PROGRESS;
    sdio_event_log(slot, SDD_EVENT_CMD, cmd);

    slot_set_bus_freq(slot, slot->clock_freq);
    slot->clock_freq = slotp->current_clock_freq;

    return slot->start_cmd(slot, cmd);
}

/**
 * Set the maximum bus frequency for this slot.
 *
 * High speed mode is enabled (or disabled) on the card if required.
 *
 * @param slot the slot.
 * @param max_freq maximum bus frequency in Hz.
 */
void slot_set_max_bus_freq(struct sdio_slot *slot, int freq)
{
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_dev *f0 = slotp->functions[0];

    if (slotp->supports_high_speed) {
        if (freq > SDIO_CLOCK_FREQ_NORMAL_SPD
            && slot->clock_freq <= SDIO_CLOCK_FREQ_NORMAL_SPD) {
            f0->io->write8(f0, 0, SDIO_CCCR_HIGH_SPEED, SDIO_CCCR_HIGH_SPEED_EHS);
        }
        if (freq <= SDIO_CLOCK_FREQ_NORMAL_SPD
            && slot->clock_freq > SDIO_CLOCK_FREQ_NORMAL_SPD) {
            f0->io->write8(f0, 0, SDIO_CCCR_HIGH_SPEED, 0x00);
        }
    }

    sdio_event_log(slot, SDD_EVENT_CLOCK_FREQ, freq);
    slot->clock_freq = freq;
}

/**
 * Set the bus frequency.
 *
 * @param slot the slot to perform the command.
 * @param freq frequence value.
 */
void slot_set_bus_freq(struct sdio_slot *slot, int freq)
{
    struct sdio_slot_priv *slotp = slot->priv;

    /* set clock rate and start it if it isn't already running at the
     * correct speed. */
    
    if (slotp->current_clock_freq != freq) {
        if (slot->set_bus_freq != NULL) {
            slotp->current_clock_freq = slot->set_bus_freq(slot, freq);
        }
    }
}


/**
 * Set a slot's bus width.
 *
 * The requested width is set on both the host and the card.
 *
 * @param slot the slot.
 * @param bus_width the new bus width.
 *
 * @return 0 on success
 * @return -ve on failure.
 */
int slot_set_bus_width(struct sdio_slot *slot, int bus_width)
{
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_dev *f0 = slotp->functions[0];
    int ret;
    uint8_t bus_iface_cntl;

    if (slot->set_bus_width) {
        ret = slot->set_bus_width(slot, bus_width);
        if (ret < 0) {
            goto error;
        }
    }

    ret = f0->io->read8(f0, 0, SDIO_CCCR_BUS_IFACE_CNTL, &bus_iface_cntl);
    if (ret < 0) {
        goto error;
    }
    if (bus_width == 4) {
        bus_iface_cntl |= SDIO_CCCR_BUS_IFACE_CNTL_4BIT_BUS;
    } else {
        bus_iface_cntl &= ~SDIO_CCCR_BUS_IFACE_CNTL_4BIT_BUS;
    }
    ret = f0->io->write8(f0, 0, SDIO_CCCR_BUS_IFACE_CNTL, bus_iface_cntl);
    if (ret < 0) {
        goto error;
    }

    slot->bus_width = bus_width;
    return 0;

  error:
    /* Try to recover from errors by restoring the old bus width. */
    if (slot->set_bus_width) {
        slot->set_bus_width(slot, slot->bus_width);
    }
    return ret;
}
