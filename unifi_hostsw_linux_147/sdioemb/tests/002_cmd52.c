/*
 * 002_cmd52 - test CMD52 reads and writes.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests CMD52 reads and writes using the CSR_FROM_HOST_SCRATCH0
 * register.
 */
#include "sdd_test.h"

static const struct sdd_test_card supported_cards[] = {
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_BC6, "BC6", },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH_D00, "dash (d00)", },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH, "dash", },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_CINDERELLA, "cinderella", },
    { },
};

static void test_002_cmd52(sdio_uif_t uif, void *arg)
{
    int i;
    int ret;

    sdd_test_check_cards(supported_cards);

    for (i = 0; i < sdd_test_max_iters(); i++) {
        uint8_t r;

        ret = sdio_write8(uif, 0, SDIO_CSR_FROM_HOST_SCRATCH0, i & 0xff);
        if (ret < 0) {
            sdd_test_fail("sdio_write8");
        }

        ret = sdio_read8(uif, 0, SDIO_CSR_FROM_HOST_SCRATCH0, &r);
        if (ret < 0) {
            sdd_test_fail("sdio_read8");
        }
        if (r != (i & 0xff)) {
            sdd_test_fail("data read (%d) != expected value (%d)", r, i & 0xff);
        }
    }

    sdd_test_pass();
}

int main(int argc, char *argv[])
{
    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_002_cmd52, NULL, NULL, NULL);
}
