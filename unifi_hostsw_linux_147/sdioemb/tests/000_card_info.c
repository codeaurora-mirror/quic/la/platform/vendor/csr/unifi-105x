/*
 * 000_card_info - test reading card info.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests reading manufacturer ID, card ID, number of functions and
 * function interface and max block size.
 */
#include "sdd_test.h"

struct card_info {
    uint16_t manf_id;
    uint16_t card_id;
    int num_funcs;
    struct {
        uint8_t std_if;
        int max_block_size;
    } func[SDIO_MAX_FUNCTIONS];
};

static struct card_info card_info[] = {
    {
        .manf_id   = SDIO_MANF_ID_CSR,
        .card_id   = SDIO_CARD_ID_CSR_BC6,
        .num_funcs = 2,
        .func[0] = {
            .std_if         = 0,
            .max_block_size = 0,
        },
        .func[1] = {
            .std_if         = SDIO_STD_IFACE_BT_TYPE_A,
            .max_block_size = 512,
        },
        .func[2] = {
            .std_if         = 0,
            .max_block_size = 0,
        },
    },
};

#define NUM_CARDS (sizeof(card_info)/sizeof(card_info[0]))

static void test_000_card_info(sdio_uif_t uif, void *arg)
{
    uint16_t manf_id;
    uint16_t card_id;
    int num_funcs;
    int c, f;

    manf_id = sdio_manf_id(uif);
    if (manf_id == 0)
        sdd_test_fail("manufacturer ID == 0");
    card_id = sdio_card_id(uif);
    if (card_id == 0)
        sdd_test_fail("card ID == 0");

    for (c = 0; c < NUM_CARDS; c++) {
        if (manf_id == card_info[c].manf_id
            && card_id == card_info[c].card_id) {
            break;
        }
    }
    if (c == NUM_CARDS) {
        sdd_test_abort("card %04:%04x not recognized", manf_id, card_id);
    }

    num_funcs = sdio_num_functions(uif);
    if (num_funcs != card_info[c].num_funcs) {
        sdd_test_fail("num funcs (%d) != expected (%d)",
                      num_funcs, card_info[c].num_funcs);
    }

    for (f = 0; f <= num_funcs; f++) {
        uint8_t std_if;
        int max_block_size;

        std_if = sdio_std_if(uif, f);
        if (std_if != card_info[c].func[f].std_if) {
            sdd_test_fail("F%d: std if (%02x) != expected (%02x)",
                          f, std_if, card_info[c].func[f].std_if);
        }

        max_block_size = sdio_max_block_size(uif, f);
        if (max_block_size != card_info[c].func[f].max_block_size) {
            sdd_test_fail("F%d: max block size (%d) != expected (%d)",
                          f, max_block_size, card_info[c].func[f].max_block_size);
        }
    }

    sdd_test_pass();
}

int main(int argc, char *argv[])
{
    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_000_card_info, NULL, NULL, NULL);
}
