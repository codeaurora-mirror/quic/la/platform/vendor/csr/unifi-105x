/*
 * 006_cmd53 - test CMD53 reads and writes.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests CMD53 reads and writes using some buffer handles available
 * via the generic function.
 */
#include <string.h>

#include "sdd_test.h"

struct test_card {
    int      generic_func;
    uint32_t sdio_mode;
    int      buffer_hdl_write;
    int      buffer_hdl_read;
};

#define DASH_SDIO_MODE (0xf935 << 1)

static struct test_card dash_data = {
    .generic_func     = 2,
    .sdio_mode        = DASH_SDIO_MODE,
    .buffer_hdl_write = 0x12,
    .buffer_hdl_read  = 0x13,
};

static struct test_card cinderella_data = {
    .generic_func     = 1,
    .sdio_mode        = 0,
    .buffer_hdl_write = 0x1,
    .buffer_hdl_read  = 0x1,
};

static const struct sdd_test_card supported_cards[] = {
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH_D00, "dash (d00)", &dash_data },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH, "dash", &dash_data },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_CINDERELLA, "cinderella", &cinderella_data },
    { },
};


#define BUFFER_LEN (16*1024)


static void test_006_cmd53(sdio_uif_t uif, void *arg)
{
    struct test_card *card;
    size_t block_size;
    int i, l;
    int ret;
    uint8_t *buf_out, *buf_in;

    buf_out = malloc(2*BUFFER_LEN);
    if (!buf_out) {
        sdd_test_abort("transfer buffer");
    }
    buf_in = buf_out + BUFFER_LEN;

    card = sdd_test_check_cards(supported_cards);

    block_size = sdio_block_size(uif, card->generic_func);

    sdio_write8(uif, 0, SDIO_CCCR_IO_EN, 1 << card->generic_func);
    if (card->sdio_mode) {
        sdio_write8(uif, card->generic_func, card->sdio_mode, 0x00);
    }

    for (i = 0; i < sdd_test_max_iters(); i++) {
        int b;

        for (l = 4; l <= 4096; l++) {
            for (b = 0; b < l; b++) {
                buf_out[b] = rand()/(int)(((unsigned)RAND_MAX + 1) / 256);
                buf_in[b] = 0xaa;
            }

            ret = sdio_write(uif, card->generic_func, card->buffer_hdl_write,
                             buf_out, l, block_size);
            if (ret < 0) {
                sdd_test_fail("sdio_write");
            }

            ret = sdio_read(uif, card->generic_func, card->buffer_hdl_read,
                            buf_in, l, block_size);
            if (ret < 0) {
                sdd_test_fail("sdio_read");
            }
            if (sdio_card_id(uif) != SDIO_CARD_ID_CSR_CINDERELLA) {
                if (memcmp(buf_in, buf_out, l) != 0) {
                    sdd_test_fail("read buffer != written buffer");
                }
            }
        }
    }

    if (card->sdio_mode) {
        sdio_write8(uif, card->generic_func, card->sdio_mode, 0x01);
    }
    if (sdio_card_id(uif) == SDIO_CARD_ID_CSR_CINDERELLA) {
        sdd_test_abort("commands successful but data not verified");
    }
    sdd_test_pass();
}

int main(int argc, char *argv[])
{
    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_006_cmd53, NULL, NULL, NULL);
}
