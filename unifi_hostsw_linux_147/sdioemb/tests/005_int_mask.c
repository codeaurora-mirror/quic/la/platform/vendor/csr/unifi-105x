/*
 * 005_int_mask - test interrupt mask and unmask.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests interrupts can be correctly masked and unmasked by counting
 * the number of times the interrupt handler is called.
 */
#include <time.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>

#include "sdd_test.h"

struct test_card {
    int generic_func;
    uint32_t sdio_host_int;
};

#define BC6_SDIO_HOST_INT  (0xFC24 << 1)
#define DASH_SDIO_HOST_INT (0xF92F << 1)

static struct test_card bc6_data = {
    .generic_func  = 2,
    .sdio_host_int = BC6_SDIO_HOST_INT,
};

static struct test_card dash_data = {
    .generic_func  = 2,
    .sdio_host_int = DASH_SDIO_HOST_INT,
};

static const struct sdd_test_card supported_cards[] = {
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_BC6, "BC6", &bc6_data },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH_D00, "dash (d00)", &dash_data },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH, "dash", &dash_data },
    { },
};

struct state {
    sem_t int_sem;
    int int_count;
};

static void int_handler(sdio_uif_t uif, void *arg)
{
    struct state *state = arg;

    state->int_count++;

    sdio_interrupt_mask(uif);

    sem_post(&state->int_sem);
}

static void test_005_int_mask(sdio_uif_t uif, void *arg)
{
    struct test_card *card;
    struct state *state = arg;
    struct timespec timeout;
    int i;
    int ret;

    card = sdd_test_check_cards(supported_cards);

    sdio_write8(uif, 0, SDIO_CCCR_IO_EN, 1 << card->generic_func);

    for (i = 0; i < sdd_test_max_iters(); i++) {
        if (sdio_write8(uif, card->generic_func, card->sdio_host_int, 1) < 0) {
            sdd_test_abort("write to SDIO_HOST_INT");
        }

        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 5;
        ret = sem_timedwait(&state->int_sem, &timeout);
        if (ret < 0) {
            if (errno == ETIMEDOUT) {
                sdd_test_fail("no interrupt received");
            }
            sdd_test_abort("waiting for interrupt");
        }

        /* Wait a bit before clearing the interrupt, if interrupts
           weren't masked properly the interrupt handler will be
           spammed during this time. */
        usleep(50000);
        sdio_write8(uif, 0, SDIO_CSR_HOST_INT, 1);
        sdio_interrupt_unmask(uif);
    }

    if (state->int_count != sdd_test_max_iters()) {
        sdd_test_fail("numer of interrupts (%d) not expected (%d)",
                      state->int_count, sdd_test_max_iters());
    }

    sdd_test_pass();
}

int main(int argc, char *argv[])
{
    struct state state;

    sem_init(&state.int_sem, 0, 0);
    state.int_count = 0;

    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_005_int_mask, &state,
                        int_handler, &state);
}
