/*
 * 004_interrupt - test SDIO interrupt
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests SDIO interrupts.
 */
#include <time.h>
#include <semaphore.h>
#include <errno.h>

#include "sdd_test.h"

struct test_card {
    int generic_func;
    uint32_t sdio_host_int;
};

#define BC6_SDIO_HOST_INT  (0xFC24 << 1)
#define DASH_SDIO_HOST_INT (0xF92F << 1)

static struct test_card bc6_data = {
    .generic_func  = 2,
    .sdio_host_int = BC6_SDIO_HOST_INT,
};

static struct test_card dash_data = {
    .generic_func  = 2,
    .sdio_host_int = DASH_SDIO_HOST_INT,
};

static const struct sdd_test_card supported_cards[] = {
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_BC6, "BC6", &bc6_data },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH_D00, "dash (d00)", &dash_data },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH, "dash", &dash_data },
    { },
};

struct state {
    sem_t int_sem;
};

static void clear_int(sdio_uif_t uif)
{
    sdio_write8(uif, 0, SDIO_CSR_HOST_INT, 1);
}

static void int_handler(sdio_uif_t uif, void *arg)
{
    struct state *state = arg;

    clear_int(uif);
    sem_post(&state->int_sem);
}

static void test_004_interrupt(sdio_uif_t uif, void *arg)
{
    struct test_card *card;
    struct state *state = arg;
    struct timespec timeout;
    int i;
    int failed = 0;
    int ret;

    card = sdd_test_check_cards(supported_cards);

    sdio_write8(uif, 0, SDIO_CCCR_IO_EN, 1 << card->generic_func);

    for (i = 0; i < sdd_test_max_iters(); i++) {
        if (sdio_write8(uif, card->generic_func, card->sdio_host_int, 1) < 0) {
            sdd_test_abort("write to SDIO_HOST_INT");
        }

        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 1;
        ret = sem_timedwait(&state->int_sem, &timeout);
        if (ret < 0) {
            if (errno == ETIMEDOUT) {
                clear_int(uif);
                failed++;
            } else {
                sdd_test_abort("waiting for interrupt");
            }
        }
    }
    if (failed) {
        sdd_test_fail("%d out of %d interrupts missed", failed, sdd_test_max_iters());
    }

    sdd_test_pass();
}

int main(int argc, char *argv[])
{
    struct state state;

    sem_init(&state.int_sem, 0, 0);

    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_004_interrupt, &state,
                        int_handler, &state);
}
