/*
 * Linux i.MX27 slot driver platform data definitions.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef _SLOT_IMX27_LX_H
#define _SLOT_IMX27_LX_H

#include <sdioemb/slot_api.h>

struct imx_sdio_plat_data {
    int max_bus_width;
    int (*card_power)(struct platform_device *, enum sdio_power);
    int (*card_present)(struct platform_device *);
};

#endif /* #ifndef _SLOT_IMX27_LX_H */
