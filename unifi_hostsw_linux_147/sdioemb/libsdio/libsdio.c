/*
 * SDIO Userspace Interface library.
 *
 * Copyright (C) 2007-2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>

#include <linux/sdioemb/uif.h>
#include <sdioemb/libsdio.h>

#include "libsdio_internal.h"

static void *int_thread_func(void *arg);

sdio_uif_t sdio_open(const char *dev_filename,
                     void (*int_handler)(struct sdio_uif *, void *), void *arg)
{
    struct sdio_uif *uif;
    int ret;

    uif = malloc(sizeof(struct sdio_uif));
    if (!uif)
        return NULL;

    uif->int_handler = int_handler;
    uif->arg         = arg;

    pthread_mutex_init(&uif->mutex, NULL);
    pthread_cond_init(&uif->int_cond, NULL);
    uif->int_masked = 0;

    uif->fd = open(dev_filename, O_RDWR);
    if (uif->fd < 0) {
        free(uif);
        return NULL;
    }

    ret = async_thread_start(uif);
    if (ret < 0) {
        free(uif);
        return NULL;
    }

    if (uif->int_handler) {
        int ret = pthread_create(&uif->int_thread, NULL, int_thread_func, uif);
        if (ret) {
            close(uif->fd);
            free(uif);
            return NULL;
        }
    }

    return uif;
}


void sdio_close(sdio_uif_t uif)
{
    if (uif == NULL) {
        return;
    }
    if (uif->int_handler) {
        pthread_cancel(uif->int_thread);
        pthread_join(uif->int_thread, NULL);
    }
    async_thread_stop(uif);
    close(uif->fd);
    pthread_mutex_destroy(&uif->mutex);
    pthread_cond_destroy(&uif->int_cond);
    free(uif);
}

int sdio_num_functions(sdio_uif_t uif)
{
    return ioctl(uif->fd, SDD_UIF_IOCQNUMFUNCS, 0);
}

int sdio_set_bus_width(sdio_uif_t uif, int bus_width)
{
    return ioctl(uif->fd, SDD_UIF_IOCTBUSWIDTH, bus_width);
}

void sdio_set_max_bus_freq(sdio_uif_t uif, int max_freq)
{
    ioctl(uif->fd, SDD_UIF_IOCTBUSFREQ, max_freq);
}

uint16_t sdio_manf_id(sdio_uif_t uif)
{
    return ioctl(uif->fd, SDD_UIF_IOCQMANFID, 0);
}

uint16_t sdio_card_id(sdio_uif_t uif)
{
    return ioctl(uif->fd, SDD_UIF_IOCQCARDID, 0);
}

uint8_t sdio_std_if(sdio_uif_t uif, int func)
{
    return ioctl(uif->fd, SDD_UIF_IOCQSTDIF, func);
}

int sdio_max_block_size(sdio_uif_t uif, int func)
{
    return ioctl(uif->fd, SDD_UIF_IOCQMAXBLKSZ, func);
}

int sdio_block_size(sdio_uif_t uif, int func)
{
    return ioctl(uif->fd, SDD_UIF_IOCQBLKSZ, func);
}

int sdio_set_block_size(sdio_uif_t uif, int func, int blksz)
{
    return ioctl(uif->fd, SDD_UIF_IOCTBLKSZ, (blksz << 8) | func);
}

int sdio_read8(sdio_uif_t uif, int func, uint32_t addr, uint8_t *data)
{
    struct sdio_uif_cmd cmd;

    cmd.type     = SDD_UIF_CMD52_READ;
    cmd.function = func;
    cmd.address  = addr;
    cmd.data     = data;
    cmd.len      = 1;

    return ioctl(uif->fd, SDD_UIF_IOCCMD, &cmd);
}

int sdio_write8(sdio_uif_t uif, int func, uint32_t addr, uint8_t data)
{
    struct sdio_uif_cmd cmd;

    cmd.type     = SDD_UIF_CMD52_WRITE;
    cmd.function = func;
    cmd.address  = addr;
    cmd.data     = &data;
    cmd.len      = 1;

    return ioctl(uif->fd, SDD_UIF_IOCCMD, &cmd);
}

int sdio_read(sdio_uif_t uif, int func, uint32_t addr, uint8_t *data,
              size_t len, int block_size)
{
    struct sdio_uif_cmd cmd;

    cmd.type       = SDD_UIF_CMD53_READ;
    cmd.function   = func;
    cmd.address    = addr;
    cmd.data       = data;
    cmd.len        = len;
    cmd.block_size = block_size;

    return ioctl(uif->fd, SDD_UIF_IOCCMD, &cmd);
}

int sdio_write(sdio_uif_t uif, int func, uint32_t addr, const uint8_t *data,
               size_t len, int block_size)
{
    struct sdio_uif_cmd cmd;

    cmd.type       = SDD_UIF_CMD53_WRITE;
    cmd.function   = func;
    cmd.address    = addr;
    cmd.data       = (uint8_t *)data; /* const cast is safe */
    cmd.len        = len;
    cmd.block_size = block_size;

    return ioctl(uif->fd, SDD_UIF_IOCCMD, &cmd);
}

int sdio_reinsert_card(sdio_uif_t uif)
{
    return ioctl(uif->fd, SDD_UIF_IOCREINSERT, 0);
}

static void *int_thread_func(void *arg)
{
    struct sdio_uif *uif = arg;

    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    for(;;) {
        int ret = ioctl(uif->fd, SDD_UIF_IOCWAITFORINT, 0);
        if (ret < 0) {
            pthread_testcancel();
            if (errno != EINTR) {
                perror("ioctl: SDD_UIF_IOCWAITFORINT: ");
                pthread_exit(0);
            }
        } else {
            uif->int_handler(uif, uif->arg);
        }

        pthread_mutex_lock(&uif->mutex);
        while (uif->int_masked) {
            pthread_cond_wait(&uif->int_cond, &uif->mutex);
        }
        pthread_mutex_unlock(&uif->mutex);
    }
    pthread_exit(0);
}

void sdio_interrupt_mask(sdio_uif_t uif)
{
    /* mask is only callable from the interrupt handler */
    assert(pthread_self() == uif->int_thread);

    pthread_mutex_lock(&uif->mutex);
    uif->int_masked = 1;
    pthread_mutex_unlock(&uif->mutex);
}

void sdio_interrupt_unmask(sdio_uif_t uif)
{
    pthread_mutex_lock(&uif->mutex);
    uif->int_masked = 0;
    pthread_mutex_unlock(&uif->mutex);

    pthread_cond_signal(&uif->int_cond);
}
