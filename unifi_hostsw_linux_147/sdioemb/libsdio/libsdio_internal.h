/*
 * libsdio internal header.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef LIBSDIO_INTERNAL_H
#define LIBSDIO_INTERNAL_H

struct cmd_queue_elem {
    struct cmd_queue_elem *next;
    sdio_io_callback_t callback;
    void *arg;
    struct sdio_uif_cmd cmd;
    uint8_t write8_data;
};

struct cmd_queue {
    struct cmd_queue_elem *head;
    struct cmd_queue_elem *tail;
    pthread_cond_t cond;
};

struct sdio_uif {
    void (*int_handler)(struct sdio_uif *, void *);
    void *arg;
    int fd;
    pthread_t int_thread;
    pthread_mutex_t mutex;
    pthread_cond_t int_cond;
    int int_masked;

    struct cmd_queue cmd_queue;
    pthread_t async_thread;
};

int async_thread_start(sdio_uif_t uif);
void async_thread_stop(sdio_uif_t uif);

#endif /* #ifndef LIBSDIO_INTERNAL_H */
